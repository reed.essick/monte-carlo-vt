"""a module to house basic condor logic
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import os
import sys
import getpass
from distutils.spawn import find_executable

from collections import defaultdict

### non-standard libraries

from .utils import event2duration
from .segments import livetime
from .dft import (DEFAULT_SEGLEN, DEFAULT_OVERLAP, DEFAULT_TUKEY_ALPHA)

try:
    from gwdistributions.backends import names as gwdist_names
except:
    gwdist_names = None

try:
    from gwdistributions import gwdio
except:
    gwdio = None

#-------------------------------------------------

DEFAULT_WINDOW = 60 # sec

DEFAULT_MODDIR = 100000 # used to divide up files into subdirectories based on gpsstart

#-------------------------------------------------

SUB = '''\
universe = %(universe)s
executable = %(exe)s
arguments = %(arguments)s
output = %(logdir)s/$(JOB).out
error = %(logdir)s/$(JOB).err
log = %(logdir)s/$(JOB).log
getenv = True
request_cpus = %(request_cpus)d
request_memory = %(request_memory)dMB
request_disk = %(request_disk)dMB
accounting_group = %(group)s
accounting_group_user = %(user)s
%(extra)squeue 1'''

#output = %(logdir)s/%(name)s-$(JOB).out
#error = %(logdir)s/%(name)s-$(JOB).err
#log = %(logdir)s/%(name)s-$(JOB).log

JOB = '''\
JOB   %(name)s %(sub)s
RETRY %(name)s %(retry)d\n'''
VARS = 'VARS  %(name)s %(vars)s\n' ### should be concatenated at the end of JOB as needed

PARENT_CHILD = 'PARENT %(parent)s CHILD %(child)s\n'

#-------------------------------------------------

DEFAULT_UNIVERSE = 'vanilla'
DEFAULT_RETRY = 0

DEFAULT_ACCOUNTING_GROUP_USER = None # automatically look this up
DEFAULT_X509 = None # do not include an X509 option
AUTOMATIC_X509_LOOKUP = 'auto'

DEFAULT_REQUEST_MEMORY = 100 # 100 MB
DEFAULT_REQUEST_CPUS = 1
DEFAULT_REQUEST_DISK = 1000 # 1000 MB

def sub(
        path,
        exe,
        args,
        logdir,
        accounting_group, 
        universe=DEFAULT_UNIVERSE,
        request_memory=DEFAULT_REQUEST_MEMORY,
        request_cpus=DEFAULT_REQUEST_CPUS,
        request_disk=DEFAULT_REQUEST_DISK,
        accounting_group_user=DEFAULT_ACCOUNTING_GROUP_USER,
        x509=DEFAULT_X509,
        verbose=False,
    ):
    """write SUB file
    """
    if verbose:
        print('writing %s SUB: %s'%(exe, path))

    if accounting_group_user is DEFAULT_ACCOUNTING_GROUP_USER:
        accounting_group_user = getpass.getuser()

    # include extra/optional specifications
    extra = ''

    ### include x509 proxy option
    if x509 == AUTOMATIC_X509_LOOKUP: # allow condor to automatically look this up
        extra += 'use_x509userproxy = True\n'

    elif isinstance(x509, str): # interpret this as a path to the x509 certificate
        extra += 'x509userproxy = %s\n' % x509

    elif x509 == DEFAULT_X509: # don't do anything
        pass

    else:
        raise ValueError('x509=%s not understood!' % x509)

    #---

    # write the file
    with open(path, 'w') as obj:
        obj.write(SUB%{
            'universe':universe,
            'exe':find_executable(exe),
            'arguments':' '.join(args),
            'logdir':logdir,
            'group':accounting_group,
            'user':accounting_group_user,
            'request_memory':request_memory,
            'request_cpus':request_cpus,
            'request_disk':request_disk,
            'extra':extra,
        })

    return path

#-----------

def shell(
        path,
        exe,
        args,
        verbose=False,
    ):
    """write a bash script
    """
    if verbose:
        print('writing %s : %s'%(exe, path))

    with open(path, 'w') as obj:
        obj.write('#!/bin/bash\n')
        obj.write('\n' + find_executable(exe))
        for arg in args:
            obj.write(' \\\n    '+arg)

    os.system('chmod +x '+path) # make shell script executable

    return path

#-----------

def workflow(path, scripts, verbose=False):
    """write a workflow (sequence of scripts) into a bash script
    """
    if verbose:
        print('writing workflow to : '+path)

    with open(path, 'w') as obj:
        obj.write('#!/bin/bash\n')
        for script in scripts:
            obj.write('\n%s || exit 1' % script) # make workflow exit if any jobs fail

    os.system('chmod +x '+path) # make shell script executable

    return path

#-------------------------------------------------
#
# Logic for sampling
#
#-------------------------------------------------

SAMPLE_NAME = 'mcvt-sample-%d'
DEFAULT_SAMPLE_UNIVERSE = 'vanilla'
DEFAULT_SAMPLE_REQUEST_CPUS = 1

def sample_memory_requirement(num_samples, save_vector_attributes=False):
    """these should be conservative scalings with the size of the sample set
    """
    return 100 + (1.7 if save_vector_attributes else 0.004) * num_samples # MB

def sample_disk_requirement(num_samples, save_vector_attributes=False):
    """these should be conservative scalings with the size of the sample set
    """
    return 100 + int((2 if save_vector_attributes else 0.0005) * num_samples) # MB

def sample_sub(
        config,
        num_samples,
        logdir,
        accounting_group,
        store_logprob=False,
        skip_factored_logprob=False,
        save_vector_attributes=False,
        ignore_conditionals=False,
        gps_range=None,
        seed=None,
        backend=None,
        verbose=None,
        Verbose=None,
        debug=None,
        accounting_group_user=DEFAULT_ACCOUNTING_GROUP_USER,
        universe=DEFAULT_SAMPLE_UNIVERSE,
        request_memory=None, ### scale this automatically with the number of samples requested
        request_cpus=DEFAULT_SAMPLE_REQUEST_CPUS,
        request_disk=None, ### scale this automatically with the number of samples requested
    ):
    """write a SUB file for mcvt-sample jobs
    """
    path = os.path.join(logdir, 'mcvt-sample.sub')

    # set up arguments
    sample_args = [
        config,
        str(num_samples),
        '--output-path', '$(SAMPLE_OUTPUT)',
    ]

    if gps_range:
        sample_args += ['--gps-range'] + [str(_) for _ in gps_range]

    if seed is not None:
        sample_args += ['--seed', '$(SEED)']

    if backend is not None:
        sample_args += ['--backend', backend]

    if store_logprob:
        sample_args += ['--store-logprob']

    if skip_factored_logprob:
        sample_args += ['--skip-factored-logprob']

    if save_vector_attributes:
        sample_args += ['--save-vector-attributes']
        if not isinstance(save_vector_attributes, bool):
            sample_args += save_vector_attributes

    if ignore_conditionals:
        sample_args += ['--ignore-conditionals']

    if debug:
        sample_args += ['--debug']

    elif Verbose:
        sample_args += ['--Verbose']

    elif verbose:
        sample_args += ['--verbose']

    ### figure out how much disk to request
    ### these should be conservative scalings with the size of the sample set
    if request_memory is None:
        request_memory = sample_memory_requirement(num_samples, save_vector_attributes=save_vector_attributes)

    if request_disk is None:
        request_disk = sample_disk_requirement(num_samples, save_vector_attributes=save_vector_attributes)

    # write the file
    return sub(
        path,
        'mcvt-sample',
        sample_args,
        '$(LOGDIR)',
        accounting_group,
        accounting_group_user=accounting_group_user,
        universe=universe,
        request_memory=request_memory,
        request_cpus=request_cpus,
        request_disk=request_disk,
        verbose=True,
    )

#-----------

def sample_shell(
        sample_name,
        output,
        config,
        num_samples_per_proc,
        logdir,
        store_logprob=False,
        skip_factored_logprob=False,
        save_vector_attributes=False,
        ignore_conditionals=False,
        gps_range=None,
        seed=None,
        backend=None,
        verbose=None,
        Verbose=None,
        debug=None,
    ):
    """write a shell script for mcvt-sample
    """
    path = os.path.join(logdir, sample_name+'.sh')

    # set up arguments
    sample_args = [
        config,
        str(num_samples_per_proc),
        '--output-path', output,
    ]

    if gps_range:
        sample_args += ['--gps-range' + ' '.join(str(_) for _ in gps_range)]

    if seed is not None:
        sample_args += ['--seed', str(seed)]

    if backend is not None:
        sample_args += ['--backend', backend]

    if store_logprob:
        sample_args += ['--store-logprob']

    if skip_factored_logprob:
        sample_args += ['--skip-factored-logprob']

    if save_vector_attributes:
        sample_args += [ '--save-vector-attributes']
        if not isinstance(save_vector_attributes, bool):
            sample_args += save_vector_attributes

    if ignore_conditionals:
        sample_args += ['--ignore-conditionals']

    if debug:
        sample_args += ['--debug']

    elif Verbose:
        sample_args += ['--Verbose']

    elif verbose:
        sample_args += ['--verbose']

    # write the file
    return shell(
        path,
        'mcvt-sample',
        sample_args,
        verbose=True,
    )

#------------------------

CONCATENATE_NAME = 'mcvt-concatenate-sample-%s'
DEFAULT_CONCATENATE_UNIVERSE = 'vanilla'
DEFAULT_CONCATENATE_REQUEST_MEMORY = 200 # 200 MB should hopefully be enough for most jobs?
DEFAULT_CONCATENATE_REQUEST_CPUS = 1
DEFAULT_CONCATENATE_REQUEST_DISK = 1000 # 1000 MB should be enough for most jobs

def concatenate_sub(
        logdir,
        accounting_group,
        cache=False,
        verbose=False,
        Verbose=False,
        retain_individual_output=False,
        accounting_group_user=DEFAULT_ACCOUNTING_GROUP_USER,
        universe=DEFAULT_CONCATENATE_UNIVERSE,
        request_memory=DEFAULT_CONCATENATE_REQUEST_MEMORY,
        request_cpus=DEFAULT_CONCATENATE_REQUEST_CPUS,
        request_disk=DEFAULT_CONCATENATE_REQUEST_DISK,
    ):
    """write a SUB for mcvt-concatenate-sample jobs
    """
    path = os.path.join(logdir, 'mcvt-concatenate-sample.sub')

    # set up arguments
    concatenate_args = ['$(CONCATENATED_OUTPUT_PATH)', '$(INDIVIDUAL_SAMPLE_OUTPUT)']

    if cache is not None:
        concatenate_args += ['--cache', '$(CACHE)']

    if Verbose:
        concatenate_args += ['--Verbose']

    elif verbose:
        concatenate_args += ['--verbose']

    if not retain_individual_output:
        concatenate_args += ['--remove-source']

    # figure out how much memory we want
    if request_memory is None:
        request_memory = 500000 ### 500 MB should be enough for most jobs 

    # write the file
    return sub(
        path,
        'mcvt-concatenate-sample',
        concatenate_args,
        '$(LOGDIR)',
        accounting_group,
        accounting_group_user=accounting_group_user,
        universe=universe,
        request_memory=request_memory,
        request_cpus=request_cpus,
        request_disk=request_disk,
        verbose=True,
    )

#-----------

def concatenate_shell(
        concatenate_name,
        individual_sample_output, # should be an iterable
        output,
        logdir,
        cache=None,
        verbose=False,
        Verbose=False,
        retain_individual_output=False,
    ):
    """write a shell script for mcvt-concatenate-sample
    """
    path = os.path.join(logdir, concatenate_name+'.sh')

    # set up arguments
    concatenate_args = [output] + list(individual_sample_output)

    if Verbose:
        concatenate_args += ['--Verbose']

    elif verbose:
        concatenate_args += ['--verbose']

    if cache is not None:
        concatenate_args += ['--cache', cache]

    if not retain_individual_output:
        concatenate_args += ['--remove-source']

    # write the file
    return shell(
        path,
        'mcvt-concatenate-sample',
        concatenate_args,
        verbose=True,
    )

#------------------------

SUMMARIZE_NAME = 'mcvt-summarize-sample'
DEFAULT_SUMMARIZE_UNIVERSE = 'vanilla'
DEFAULT_SUMMARIZE_REQUEST_MEMORY = 100 # 100 MB should be enough for most jobs
DEFAULT_SUMMARIZE_REQUEST_CPUS = 1
DEFAULT_SUMMARIZE_REQUEST_DISK = 1000 # 1000 MB should be enough for most jobs

def summarize_memory_requirement(num_samples):
    return DEFAULT_SUMMARIZE_REQUEST_MEMORY ### FIXME: scale this with the sample size?

def summarize_disk_requirement(num_samples):
    return DEFAULT_SUMMARIZE_REQUEST_DISK ### FIXME: scale this with the sample size?

def summarize_sub(
        output_dir,
        logdir,
        accounting_group,
        snr_thresholds=[],
        columns=[],
        include_ndet=False,
        include_neff=False,
        verbose=False,
        Verbose=False,
        universe=DEFAULT_SUMMARIZE_UNIVERSE,
        accounting_group_user=DEFAULT_ACCOUNTING_GROUP_USER,
        request_memory=DEFAULT_SUMMARIZE_REQUEST_MEMORY,
        request_cpus=DEFAULT_SUMMARIZE_REQUEST_CPUS,
        request_disk=DEFAULT_SUMMARIZE_REQUEST_DISK,
    ):
    """write a SUB for mcvt-summarize-sample jobs
    """
    path = os.path.join(logdir, 'mcvt-summarize-sample.sub')

    # set up arguments
    summarize_args = [
        '$(CONCATENATED_OUTPUT_PATH)',
        '--output-dir', output_dir,
        '--plot-detected-distribution',
    ]

    for key, val in snr_thresholds:
        summarize_args += ['--snr-threshold', key, val]

    for column, label, log in columns:
        summarize_args += ['--column', column]
        if label is not None:
            summarize_args += ['--label', column, label]
        if log:
            summarize_args += ['--log', column]

    if include_ndet:
        summarize_args += ['--include-ndet']

    if include_neff:
        summarize_args += ['--include-neff']

    if Verbose:
        summarize_args += ['--Verbose']

    elif verbose:
        summarize_args += ['--verbose']

    # write the file
    return sub(
        path,
        'mcvt-summarize-sample',
        summarize_args,
        logdir,
        accounting_group,
        accounting_group_user=accounting_group_user,
        universe=universe,
        request_memory=request_memory,
        request_cpus=request_cpus,
        request_disk=request_disk,
        verbose=True,
    )

#-----------

def summarize_shell(
        output_path,
        logdir,
        snr_thresholds=[],
        columns=[],
        include_ndet=False,
        include_neff=False,
        verbose=False,
        Verbose=False,
    ):
    """write a shell script for mcvt-summarize-sample
    """

    path = os.path.join(logdir, SUMMARIZE_NAME+'.sh')

    # set up arguments
    summarize_args = [
        output_path,
        '--output-dir', os.path.dirname(output_dir),
        '--plot-detected-distribution',
    ]

    for key, val in snr_thresholds:
        summarize_args += ['--snr-threshold', key, val]

    for column, label, log in columns:
        summarize_args += ['--column', column]
        if label is not None:
            summarize_args += ['--label', column, label]
        if log:
            summarize_args += ['--log', column]

    if include_ndet:
        summarize_args += ['--include-ndet']

    if include_neff:
        summarize_args += ['--include-neff']

    if Verbose:
        summarize_args += ['--Verbose']

    elif verbose:
        summarize_args += ['--verbose']

    # write the file
    return shell(
        path,
        'mcvt-summarize-sample',
        summarize_args,
        verbose=True,
    )

#-------------------------------------------------

def sample_workflow(scripts, logdir):
    path = os.path.join(logdir, 'mcvt-condor-sample.sh')
    return workflow(path, scripts, verbose=True)

NUM_SAMPLE_JOBS_PER_DIR = 1000

def sample_dag(
        config,
        num_samples_per_proc,
        num_proc,
        output_path,
        logdir,
        accounting_group,
        store_logprob=False,
        skip_factored_logprob=False,
        save_vector_attributes=False,
        ignore_conditionals=False,
        gps_range=None,
        seed=None,
        backend=None,
        verbose=False,
        Verbose=False,
        debug=False,
        generate_shell_scripts=True,
        retain_individual_output=False,
        summarize_sample=False,
        snr_thresholds=[],
        columns=[],
        include_ndet=False,        
        include_neff=False,        
        retry=DEFAULT_RETRY,
        accounting_group_user=DEFAULT_ACCOUNTING_GROUP_USER,
        sample_universe=DEFAULT_SAMPLE_UNIVERSE,
        sample_request_cpus=DEFAULT_REQUEST_CPUS,
        sample_request_memory=None,
        sample_request_disk=None,
        concatenate_universe=DEFAULT_CONCATENATE_UNIVERSE,
        concatenate_request_cpus=DEFAULT_REQUEST_CPUS,
        concatenate_request_memory=None,
        concatenate_request_disk=None,
        summarize_universe=DEFAULT_SUMMARIZE_UNIVERSE,
        summarize_request_cpus=DEFAULT_REQUEST_CPUS,
        summarize_request_memory=None,
        summarize_request_disk=None,
    ):
    """write a DAG for the sample+concatenate workflow
    """

    # make sure we have full paths

    config = os.path.abspath(config)
    output_path = os.path.abspath(output_path)
    logdir = os.path.abspath(logdir)

    # make sure that directories exist
    os.makedirs(os.path.dirname(output_path), exist_ok=True)
    os.makedirs(logdir, exist_ok=True)

    ### handle logic to sample sequentially if samples are to be regularly spaced
    sequential_sampling = gps_range is not None
    if sequential_sampling:
        gps_start, gps_end = gps_range
        gps_dur = (gps_end - gps_start) / num_proc

        gps_range = ['$(GPS_START)', '$(GPS_END)'] ### pass to sample_sub so we can grab VARS from dag

    # write sample SUB

    if sample_request_memory is None:
        sample_request_memory = sample_memory_requirement(
            num_samples_per_proc,
            save_vector_attributes=save_vector_attributes,
        )

    if sample_request_disk is None:
        sample_request_disk = sample_disk_requirement(
            num_samples_per_proc,
            save_vector_attributes=save_vector_attributes,
        )

    sample = sample_sub(
        config,
        num_samples_per_proc,
        logdir,
        accounting_group,
        accounting_group_user=accounting_group_user,
        universe=sample_universe,
        request_memory=sample_request_memory,
        request_cpus=sample_request_cpus,
        request_disk=sample_request_disk,
        store_logprob=store_logprob,
        skip_factored_logprob=skip_factored_logprob,
        save_vector_attributes=save_vector_attributes,
        ignore_conditionals=ignore_conditionals,
        gps_range=gps_range,
        seed=seed,
        backend=backend,
        verbose=verbose,
        Verbose=Verbose,
        debug=debug,
    )

    # write concatenate SUB

    if concatenate_request_memory is None:
        concatenate_request_memory = sample_memory_requirement(
            num_samples_per_proc*num_proc,
            save_vector_attributes=save_vector_attributes,
        )

    if concatenate_request_disk is None:
        concatenate_request_disk = sample_disk_requirement(
            num_samples_per_proc*num_proc,
            save_vector_attributes=save_vector_attributes,
        )

    concatenate = concatenate_sub(
        logdir,
        accounting_group,
        accounting_group_user=accounting_group_user,
        universe=concatenate_universe,
        request_memory=concatenate_request_memory,
        request_cpus=concatenate_request_cpus,
        request_disk=concatenate_request_disk,
        cache=True,
        verbose=verbose,
        Verbose=Verbose,
        retain_individual_output=retain_individual_output,
    )

    if summarize_sample: # write summarize SUB

        if summarize_request_memory is None:
            summarize_request_memory = summarize_memory_requirement(num_samples_per_proc*num_proc)

        if summarize_request_disk is None:
            summarize_request_disk = summarize_disk_requirement(num_samples_per_proc*num_proc)

        summarize = summarize_sub(
            os.path.dirname(output_path),
            logdir,
            accounting_group,
            accounting_group_user=accounting_group_user,
            universe=summarize_universe,
            request_memory=summarize_request_memory,
            request_cpus=summarize_request_cpus,
            request_disk=summarize_request_disk,
            snr_thresholds=snr_thresholds,
            columns=columns,
            include_ndet=include_ndet,
            include_neff=include_neff,
            verbose=verbose,
            Verbose=Verbose,
        )

    # write DAG
    dag = os.path.join(logdir, 'mcvt-condor-sample.dag')
    print('writing mcvt-condor-sample DAG: '+dag)

    if generate_shell_scripts:
        scripts = [] # used to record the shell scripts for the whole workflow

    with open(dag, 'w') as obj:

        # write one job for each proc
        sample_output = defaultdict(list)
 
        for proc in range(num_proc):

            sample_name = SAMPLE_NAME % proc

            moddir = proc // NUM_SAMPLE_JOBS_PER_DIR
            this_logdir = os.path.join(logdir, 'mcvt-sample-%06d' % moddir)
            os.makedirs(this_logdir, exist_ok=True)

            if sequential_sampling: ### include VARS to define gps range for this job
                output = os.path.join(this_logdir, 'mcvt-sample-%d-%d.hdf'%(gps_start, gps_dur))
                extra_vars = ' GPS_START="%.6f" GPS_END="%.6f"'%(gps_start, gps_start+gps_dur)
                gps_start += gps_dur ### increment

            else:
                output = os.path.join(this_logdir, 'mcvt-sample-%d.hdf'%proc)
                extra_vars = ''

            if seed is not None:
                this_seed = seed + proc # unique for each proc
                extra_vars = extra_vars + ' SEED="%d"' % this_seed
            else:
                this_seed = None

            obj.write(JOB % {'name':sample_name, 'sub':sample, 'retry':retry})
            obj.write(VARS % {
                'name':sample_name,
                'vars': 'SAMPLE_OUTPUT="%s" LOGDIR="%s"' % (output, this_logdir) + extra_vars,
            })

            if generate_shell_scripts:
                # write a shell script to accomplish the same job
                scripts.append(sample_shell(
                    sample_name,
                    output,
                    config,
                    num_samples_per_proc,
                    this_logdir,
                    store_logprob=store_logprob,
                    skip_factored_logprob=skip_factored_logprob,
                    save_vector_attributes=save_vector_attributes,
                    ignore_conditionals=ignore_conditionals,
                    gps_range=gps_range,
                    seed=this_seed,
                    backend=backend,
                    verbose=verbose,
                    Verbose=Verbose,
                    debug=debug,
                ))

            # add this output to the list
            sample_output[(moddir, this_logdir)].append((sample_name, output))

        ### add concatenation jobs
        if len(sample_output) == 1: # a single job, so just grab the output of mcvt-sample directly
            concatenate_output = list(sample_output.values())[0]

        else: # more than one job, so first concatenate each subdir separately
            concatenate_output = []

            for (moddir, this_logdir), samples in sample_output.items():
                concatenate_name = CONCATENATE_NAME % str(moddir)
                cache_name = os.path.join(this_logdir, concatenate_name+'-cache.txt')
                this_output_path = os.path.join(this_logdir, concatenate_name+'.hdf')

                ### write the concatenate job
                obj.write(JOB % {'name':concatenate_name, 'sub':concatenate, 'retry':retry})
                obj.write(VARS % {
                    'name':concatenate_name,
                    'vars':'CONCATENATED_OUTPUT_PATH="%s" CACHE="%s" LOGDIR="%s"' % \
                        (this_output_path, cache_name, this_logdir)
                })

                # write cache file and add parent-child relations
                with open(cache_name, 'w') as ect:
                    for sample_name, output in samples:
                        ect.write(output+'\n')
                        obj.write(PARENT_CHILD % {'parent':sample_name, 'child':concatenate_name})

                if generate_shell_scripts:
                    scripts.append(concatenate_shell(
                        concatenate_name,
                        [],
                        this_output_path,
                        this_logdir,
                        cache=cache_name,
                        verbose=verbose,
                        Verbose=Verbose,
                        retain_individual_output=retain_individual_output,
                    ))

                concatenate_output.append((concatenate_name, this_output_path))

        # now add the final concatenation job
        concatenate_name = CONCATENATE_NAME % 'cumulative'
        cache_name = os.path.join(logdir, concatenate_name+'-cache.txt')

        ### then write the concatenate job
        obj.write(JOB % {'name':concatenate_name, 'sub':concatenate, 'retry':retry})
        obj.write(VARS % {
            'name':concatenate_name,
            'vars':'CONCATENATED_OUTPUT_PATH="%s" CACHE="%s" LOGDIR="%s"' % \
                (output_path, cache_name, logdir)
        })

        with open(cache_name, 'w') as ect:
            for sample_name, output in concatenate_output:
                ect.write(output+'\n')
                obj.write(PARENT_CHILD % {'parent':sample_name, 'child':concatenate_name})

        if generate_shell_scripts:
            scripts.append(concatenate_shell(
                concatenate_name,
                [],
                output_path,
                logdir,
                cache=cache_name,
                verbose=verbose,
                Verbose=Verbose,
                retain_individual_output=retain_individual_output,
            ))

        ### finally, write sample job if requested
        if summarize_sample:
            obj.write(JOB % {'name':SUMMARIZE_NAME, 'sub':summarize, 'retry':retry})
            obj.write(VARS % {'name':SUMMARIZE_NAME, 'vars':'CONCATENATED_OUTPUT_PATH="%s"'%output_path})
            obj.write(PARENT_CHILD % {'parent':CONCATENATE_NAME, 'child':SUMMARIZE_NAME})

            if generate_shell_scripts:
                scripts.append(summarize_shell(
                    output_path,
                    logdir,
                    snr_thresholds=snr_thresholds,
                    columns=columns,
                    include_ndet=include_ndet,
                    include_neff=include_neff,
                    verbose=verbose,
                    Verbose=Verbose,
                ))

    if generate_shell_scripts:
        sample_workflow(
            scripts,
            logdir,
        )

    ### return path to DAG
    return dag

#-------------------------------------------------
#
# Logic for estimating PSDs
#
#-------------------------------------------------

ESTIMATE_PSD_FILENAME = 'mcvt-estimate-psd'
ESTIMATE_PSD_NAME = ESTIMATE_PSD_FILENAME+'-%d-%d'

def _channel2tag(channel):
    return channel.replace(':','-')

def psd_path(outdir, tag, start, stride):
    return os.path.join(
        outdir,
        'mcvt-estimate-psd%s-%d-%d.txt.gz' % (tag, start, stride),
    )

DEFAULT_ESTIMATE_PSD_UNIVERSE = 'vanilla'
DEFAULT_ESTIMATE_PSD_REQUEST_MEMORY = 1000 # 1000 MB should hopefully be enough for most jobs?
DEFAULT_ESTIMATE_PSD_REQUEST_CPUS = 1
DEFAULT_ESTIMATE_PSD_REQUEST_DISK = 1000 # 1000 MB should be enough for most jobs

#------------------------

def estimate_psd_sub(
        channel,
        framepath,
        seglen,
        overlap,
        tukey_alpha,
        logdir,
        accounting_group,
        verbose=None,
        Verbose=None,
        accounting_group_user=DEFAULT_ACCOUNTING_GROUP_USER,
        universe=DEFAULT_ESTIMATE_PSD_UNIVERSE,
        request_memory=DEFAULT_ESTIMATE_PSD_REQUEST_MEMORY,
        request_cpus=DEFAULT_ESTIMATE_PSD_REQUEST_CPUS,
        request_disk=DEFAULT_ESTIMATE_PSD_REQUEST_DISK,
        x509=DEFAULT_X509,
    ):
    """write a SUB for mcvt-estimate-psd jobs
    """
    path = os.path.join(logdir, ESTIMATE_PSD_FILENAME+'.sub')

    # set up arguments
    psd_args = [
        channel,
        '$(GPSSTART)',
        '$(GPSSTOP)',
        '--frames', framepath,
        '--seglen', '%d'%seglen,
        '--overlap', '%.6e'%overlap,
        '--tukey-alpha', '%.6e'%tukey_alpha,
        '--output-dir', '$(OUTDIR)',
        '--tag', _channel2tag(channel),
    ]

    if Verbose:
        psd_args.append('--Verbose')

    elif verbose:
        psd_args.append('--verbose')

    # write the file
    return sub(
        path,
        'mcvt-estimate-psd',
        psd_args,
        '$(LOGDIR)',
        accounting_group,
        accounting_group_user=accounting_group_user,
        universe=universe,
        request_memory=request_memory,
        request_cpus=request_cpus,
        request_disk=request_disk,
        verbose=True,
        x509=x509,
    )

#------------------------

def estimate_psd_shell(
        psd_name,
        channel,
        gpsstart,
        gpsstop,
        framepath,
        seglen,
        overlap,
        tukey_alpha,
        outdir,
        logdir,
        verbose=None,
        Verbose=None,
    ):
    path = os.path.join(logdir, psd_name+'.sh')

    psd_args = [
        channel,
        '%d'%gpsstart,
        '%d'%gpsstop,
        '--frames', framepath,
        '--seglen', '%d'%seglen,
        '--overlap', '%.6e'%overlap,
        '--tukey-alpha', '%.6e'%tukey_alpha,
        '--output-dir', outdir,
        '--tag', _channel2tag(channel),
    ]

    if Verbose:
        psd_args.append('--Verbose')

    elif verbose:
        psd_args.append('--verbose')

    # write the file
    return shell(
        path,
        'mcvt-estimate-psd',
        psd_args,
        verbose=True,
    )

#-------------------------------------------------

QUANTILE_PSD_FILENAME = "mcvt-estimate-psd-quantile"
QUANTILE_PSD_NAME = QUANTILE_PSD_FILENAME+"-%d"

DEFAULT_QUANTILE_PSD_UNIVERSE = 'vanilla'
DEFAULT_QUANTILE_PSD_REQUEST_MEMORY = 2000 # 2000 MB should hopefully be enough for most jobs?
DEFAULT_QUANTILE_PSD_REQUEST_CPUS = 1
DEFAULT_QUANTILE_PSD_REQUEST_DISK = 1000 # 1000 MB should be enough for most jobs

def quantile_psd_sub(
        logdir,
        accounting_group,
        quantiles=None,
        plot=False,
        plot_min_psd=None,
        plot_max_psd=None,
        plot_min_frequency=None,
        plot_max_frequency=None,
        verbose=False,
        Verbose=False,
        accounting_group_user=DEFAULT_ACCOUNTING_GROUP_USER,
        universe=DEFAULT_QUANTILE_PSD_UNIVERSE,
        request_memory=DEFAULT_QUANTILE_PSD_REQUEST_MEMORY,
        request_cpus=DEFAULT_QUANTILE_PSD_REQUEST_CPUS,
        request_disk=DEFAULT_QUANTILE_PSD_REQUEST_DISK,
    ):
    path = os.path.join(logdir, QUANTILE_PSD_FILENAME+'.sub')

    # set up arguments
    rep_args = ['$(QUANTILE_PSD_PATH)', '--psd-cache', '$(PSD_CACHE)']

    if quantiles is not None:
        for quantile in quantiles:
            rep_args += ['--quantile', '%.6e'%quantile]

    if plot:
        rep_args.append('--plot')

    if plot_min_psd:
        rep_args += ['--plot-min-psd', '%.6e'%plot_min_psd]

    if plot_max_psd:
        rep_args += ['--plot-max-psd', '%.6e'%plot_max_psd]

    if plot_min_frequency:
        rep_args += ['--plot-min-frequency', '%.6e'%plot_min_frequency]

    if plot_max_frequency:
        rep_args += ['--plot-max-frequency', '%.6e'%plot_max_frequency]

    if Verbose:
        rep_args.append('--Verbose')

    elif verbose:
        rep_args.append('--verbose')

    # write the file
    return sub(
        path,
        'mcvt-estimate-psd-quantile',
        rep_args,
        '$(LOGDIR)',
        accounting_group,
        accounting_group_user=accounting_group_user,
        universe=universe,
        request_memory=request_memory,
        request_cpus=request_cpus,
        request_disk=request_disk,
        verbose=True,
    )

#------------------------

def quantile_psd_shell(
        qnt_name,
        individual_psds, # should be an iterable
        output,
        logdir,
        psd_cache=None,
        quantiles=None,
        plot=False,
        plot_min_psd=None,
        plot_max_psd=None,
        plot_min_frequency=None,
        plot_max_frequency=None,
        verbose=False,
        Verbose=False,
    ):
    """write a shell script to extract PSD quantiles
    """
    path = os.path.join(logdir, qnt_name+'.sh')

    # set up arguments
    rep_args = list(individual_psds) + [output]

    if psd_cache:
        rep_args += ['--psd-cache', psd_cache]

    if quantiles is not None:
        for quantile in quantiles:
            rep_args += ['--quantile', '%.6e'%quantile]

    if plot:
        rep_args.append('--plot')

    if plot_min_psd:
        rep_args += ['--plot-min-psd', '%.6e'%plot_min_psd]

    if plot_max_psd:
        rep_args += ['--plot-max-psd', '%.6e'%plot_max_psd]

    if plot_min_frequency:
        rep_args += ['--plot-min-frequency', '%.6e'%plot_min_frequency]

    if plot_max_frequency:
        rep_args += ['--plot-max-frequency', '%.6e'%plot_max_frequency]

    if Verbose:
        rep_args.append('--Verbose')

    elif verbose:
        rep_args.append('--verbose')

    # write the file
    return shell(
        path,
        'mcvt-estimate-psd-quantile',
        rep_args,
        verbose=True,
    )

#-------------------------------------------------

COMBINE_QUANTILE_NAME = 'mcvt-combine-psd-quantile'

DEFAULT_COMBINE_QUANTILE_UNIVERSE = 'vanilla'
DEFAULT_COMBINE_QUANTILE_REQUEST_MEMORY = 2000 # 2000 MB should hopefully be enough for most jobs?
DEFAULT_COMBINE_QUANTILE_REQUEST_CPUS = 1
DEFAULT_COMBINE_QUANTILE_REQUEST_DISK = 1000 # 1000 MB should be enough for most jobs?

DEFAULT_NUM_PSD_PER_QUANTILE_JOB = 1000 # should be small enough to fit within memory request

def combine_quantile_sub(
        logdir,
        accounting_group,
        verbose=False,
        accounting_group_user=DEFAULT_ACCOUNTING_GROUP_USER,
        universe=DEFAULT_COMBINE_QUANTILE_UNIVERSE,
        request_memory=DEFAULT_COMBINE_QUANTILE_REQUEST_MEMORY,
        request_cpus=DEFAULT_COMBINE_QUANTILE_REQUEST_CPUS,
        request_disk=DEFAULT_COMBINE_QUANTILE_REQUEST_DISK,
    ):
    path = os.path.join(logdir, COMBINE_QUANTILE_NAME+'.sub')

    # set up arguments
    cmb_args = ['$(COMBINE_QNT_PATH)', '--quantile-cache', '$(QNT_CACHE)']

    if verbose:
        cmb_args.append('--verbose')

    # write the file
    return sub(
        path,
        'mcvt-combine-psd-quantile',
        cmb_args,
        logdir,
        accounting_group,
        accounting_group_user=accounting_group_user,
        universe=universe,
        request_memory=request_memory,
        request_cpus=request_cpus,
        request_disk=request_disk,
        verbose=True,
    )

def combine_quantile_shell(
        qnt_paths,
        output,
        logdir,
        qnt_cache=None,
        verbose=False,
    ):
    path = os.path.join(logdir, COMBINE_QUANTILE_NAME+'.sh')

    # set up arguments
    cmb_args = list(qnt_paths) + [output]

    if qnt_cache:
        cmb_args += ['--quantile-cache', qnt_cache]

    if verbose:
        cmb_args.append('--verbose')

    # write the file
    return shell(
        path,
        'mcvt-combine-psd-quantile',
        cmb_args,
        verbose=True,
    )

#-------------------------------------------------

REPRESENTATIVE_PSD_NAME = 'mcvt-extract-psd-quantile'

DEFAULT_REPRESENTATIVE_PSD_UNIVERSE = 'vanilla'
DEFAULT_REPRESENTATIVE_PSD_REQUEST_MEMORY = 1000 # 1000 MB
DEFAULT_REPRESENTATIVE_PSD_REQUEST_CPUS = 1
DEFAULT_REPRESENTATIVE_PSD_REQUEST_DISK = 1000 # 1000 MB

DEFAULT_REPRESENTATIVE_QUANTILE = 0.50 # median

def representative_psd_sub(
        logdir,
        accounting_group,
        verbose=False,
        accounting_group_user=None,
        universe=DEFAULT_REPRESENTATIVE_PSD_UNIVERSE,
        request_memory=DEFAULT_REPRESENTATIVE_PSD_REQUEST_MEMORY,
        request_cpus=DEFAULT_REPRESENTATIVE_PSD_REQUEST_CPUS,
        request_disk=DEFAULT_REPRESENTATIVE_PSD_REQUEST_DISK,
    ):
    path = os.path.join(logdir, REPRESENTATIVE_PSD_NAME+'.sub')

    # set up arguments
    rep_args = ['$(QUANTILE_PATH)', '$(QUANTILE)', '$(PSD_PATH)']

    if verbose:
        rep_args.append('--verbose')

    # write the file
    return sub(
        path,
        'mcvt-extract-psd-quantile',
        rep_args,
        logdir,
        accounting_group,
        accounting_group_user=accounting_group_user,
        universe=universe,
        request_memory=request_memory,
        request_cpus=request_cpus,
        request_disk=request_disk,
        verbose=True,
    )

def representative_psd_shell(
        cmb_qnt_path,
        quantile,
        rep_path,
        logdir,
        verbose=False,
    ):
    path = os.path.join(logdir, REPRESENTATIVE_PSD_NAME+'.sh')

    # set up arguments
    rep_args = [cmb_qnt_path, '%.9e'%quantile, rep_path]

    if verbose:
        rep_args.append('--verbose')

    # write the file
    return shell(
        path,
        'mcvt-extract-psd-quantile',
        rep_args,
        verbose=True,
    )

#-------------------------------------------------

def estimate_psd_workflow(scripts, logdir):
    path = os.path.join(logdir, 'mcvt-condor-estimate-psd.sh')
    return workflow(path, scripts, verbose=True)

def estimate_psd_dag(
        channel,
        framepath,
        segs,
        gpsstart,
        gpsstop,
        output_dir,
        logdir,
        accounting_group,
        accounting_group_user=DEFAULT_ACCOUNTING_GROUP_USER,
        estimate_psd_universe=DEFAULT_ESTIMATE_PSD_UNIVERSE,
        estimate_psd_request_memory=DEFAULT_ESTIMATE_PSD_REQUEST_MEMORY,
        estimate_psd_request_cpus=DEFAULT_ESTIMATE_PSD_REQUEST_CPUS,
        estimate_psd_request_disk=DEFAULT_ESTIMATE_PSD_REQUEST_DISK,
        x509=DEFAULT_X509,
        quantile_psd_universe=DEFAULT_QUANTILE_PSD_UNIVERSE,
        quantile_psd_request_memory=DEFAULT_QUANTILE_PSD_REQUEST_MEMORY,
        quantile_psd_request_cpus=DEFAULT_QUANTILE_PSD_REQUEST_CPUS,
        quantile_psd_request_disk=DEFAULT_QUANTILE_PSD_REQUEST_DISK,
        combine_quantile_universe=DEFAULT_COMBINE_QUANTILE_UNIVERSE,
        combine_quantile_request_memory=DEFAULT_COMBINE_QUANTILE_REQUEST_MEMORY,
        combine_quantile_request_cpus=DEFAULT_COMBINE_QUANTILE_REQUEST_CPUS,
        combine_quantile_request_disk=DEFAULT_COMBINE_QUANTILE_REQUEST_DISK,
        representative_psd_universe=DEFAULT_REPRESENTATIVE_PSD_UNIVERSE,
        representative_psd_request_memory=DEFAULT_REPRESENTATIVE_PSD_REQUEST_MEMORY,
        representative_psd_request_cpus=DEFAULT_REPRESENTATIVE_PSD_REQUEST_CPUS,
        representative_psd_request_disk=DEFAULT_REPRESENTATIVE_PSD_REQUEST_DISK,
        retry=DEFAULT_RETRY,
        generate_shell_scripts=False,
        window=DEFAULT_WINDOW,
        seglen=DEFAULT_SEGLEN,
        overlap=DEFAULT_OVERLAP,
        tukey_alpha=DEFAULT_TUKEY_ALPHA,
        num_psd_per_quantile_job=DEFAULT_NUM_PSD_PER_QUANTILE_JOB,
        quantiles=None,
        representative_quantile=DEFAULT_REPRESENTATIVE_QUANTILE,
        plot=False,
        plot_min_psd=None,
        plot_max_psd=None,
        plot_min_frequency=None,
        plot_max_frequency=None,
        verbose=False,
        Verbose=False,
        moddir=DEFAULT_MODDIR,
    ):

    # make sure we have full paths

    framepath = os.path.abspath(framepath)
    output_dir = os.path.abspath(output_dir)
    logdir = os.path.abspath(logdir)

    # write the basic sub files
    psd = estimate_psd_sub(
        channel,
        framepath,
        seglen,
        overlap,
        tukey_alpha,
        logdir,
        accounting_group,
        verbose=verbose,
        Verbose=verbose,
        accounting_group_user=accounting_group_user,
        universe=estimate_psd_universe,
        request_memory=estimate_psd_request_memory,
        request_cpus=estimate_psd_request_cpus,
        request_disk=estimate_psd_request_disk,
        x509=x509,
    )

    qnt_psd = quantile_psd_sub(
        logdir,
        accounting_group,
        quantiles=quantiles,
        plot=plot,
        plot_min_psd=plot_min_psd,
        plot_max_psd=plot_max_psd,
        plot_min_frequency=plot_min_frequency,
        plot_max_frequency=plot_max_frequency,
        verbose=verbose,
        Verbose=Verbose,
        accounting_group_user=accounting_group_user,
        universe=quantile_psd_universe,
        request_memory=quantile_psd_request_memory,
        request_cpus=quantile_psd_request_cpus,
        request_disk=quantile_psd_request_disk,
    )

    cmb_qnt = combine_quantile_sub(
        logdir,
        accounting_group,
        verbose=verbose,
        accounting_group_user=accounting_group_user,
        universe=combine_quantile_universe,
        request_memory=combine_quantile_request_memory,
        request_cpus=combine_quantile_request_cpus,
        request_disk=combine_quantile_request_disk,
    )

    rep_psd = representative_psd_sub(
        logdir,
        accounting_group,
        verbose=verbose,
        accounting_group_user=accounting_group_user,
        universe=representative_psd_universe,
        request_memory=representative_psd_request_memory,
        request_cpus=representative_psd_request_cpus,
        request_disk=representative_psd_request_disk,
    )

    tag = "_" + _channel2tag(channel)

    ### iterate over segments and define compute-psd jobs for each
    dag = os.path.join(logdir, 'mcvt-condor-estimate-psd.dag')
    if verbose:
        print("writing : "+dag)

    if generate_shell_scripts:
        scripts = []

    with open(dag, 'w') as obj:

        covered = 0 ### amount of time that's covered by a PSD estimate
        psd_sets = defaultdict(list)

        for segstart, segstop in segs:

            segdur = segstop - segstart
            if verbose:
                print( "    scheduling jobs for %d -> %d"%(segstart, segstop) )

            ### line-up start with integer number of windows.
            ### needed to guarantee files will line up later -> integer division!
            s = (segstart//window)*window

            if s < segstart: ### mostly likely case, but we need to check just in case
                s += window

            while s+window < segstop:

                smod = s // moddir
                sdir = 'mcvt-estimate-psd-%d' % smod

                # make hierarchical directories
                this_outdir = os.path.join(output_dir, sdir)
                os.makedirs(this_outdir, exist_ok=True)

                this_logdir = os.path.join(logdir, sdir)
                os.makedirs(this_logdir, exist_ok=True)

                # write job into dag

                psd_name = ESTIMATE_PSD_NAME % (s, window)

                obj.write(JOB % {'name':psd_name, 'sub':psd, 'retry':retry})
                obj.write(VARS % {
                    'name':psd_name,
                    'vars': 'GPSSTART="%d" GPSSTOP="%d" OUTDIR="%s" LOGDIR="%s"' \
                        % (s, s+window, this_outdir, this_logdir),
                })

                if generate_shell_scripts:
                    scripts.append(estimate_psd_shell(
                        psd_name,
                        channel,
                        s,
                        s+window,
                        framepath,
                        seglen,
                        overlap,
                        tukey_alpha,
                        this_outdir,
                        this_logdir,
                        verbose=verbose,
                        Verbose=Verbose,
                    ))

                psd_sets[(smod, sdir)].append((psd_name, psd_path(this_outdir, tag, s, window)))

                s += window
                covered += window

        # set up quantile estimation jobs
        if verbose:
            print("    scheduling quantile-estimation jobs to hierarchically combine PSDs")

        qnt_paths = []

        for (smod, sdir), psd_set in psd_sets.items():

            qnt_name = QUANTILE_PSD_NAME % smod

            if verbose:
                print("    scheduling: "+qnt_name)

            this_outdir = os.path.join(output_dir, sdir) # these should already have been created
            this_logdir = os.path.join(logdir, sdir)

            # identify names and paths for this subset of PSD jobs
            _names = []
            _paths = []
            for name, path in psd_set:
                _names.append(name)
                _paths.append(path)

            # generate a cache of PSDs for this job
            psd_cache = os.path.join(this_logdir, 'psds-%d.txt' % smod)
            if verbose:
                print("    writing individual PSD paths for quantile job (%s) to : %s" % (qnt_name, psd_cache))
            with open(psd_cache, 'w') as ect:
                ect.write('\n'.join(_paths))

            # create quantile job in DAG and shell
            qnt_psd_path = os.path.join(this_outdir, qnt_name+'.hdf')

            # add job to make PSD quantiles
            obj.write(JOB % {'name':qnt_name, 'sub':qnt_psd, 'retry':retry})
            obj.write(VARS % {
                'name':qnt_name,
                'vars': 'QUANTILE_PSD_PATH="%s" PSD_CACHE="%s"  LOGDIR="%s"' \
                    % (qnt_psd_path, psd_cache, this_logdir),
            })

            # add parent-child relationships
            for name in _names:
                obj.write(PARENT_CHILD % {'parent':name, 'child':qnt_name})

            # generate shell script
            if generate_shell_scripts:
                scripts.append(quantile_psd_shell(
                    qnt_name,
                    [], # psd_paths, should be an iterable
                    qnt_psd_path,
                    this_logdir,
                    psd_cache=psd_cache,
                    quantiles=quantiles,
                    plot=plot,
                    plot_min_psd=plot_min_psd,
                    plot_max_psd=plot_max_psd,
                    plot_min_frequency=plot_min_frequency,
                    plot_max_frequency=plot_max_frequency,
                    verbose=verbose,
                    Verbose=Verbose,
                ))

            # increment counter
            qnt_paths.append((qnt_name, qnt_psd_path))

        # scheduling job for to combine quantiles
        if len(qnt_paths) == 1:
            if verbose:
                print("    only a single quantile job! no need to combine quantiles")
            cmb_qnt_name, cmb_qnt_path = qnt_paths[0]

        else:
            cmb_qnt_name = COMBINE_QUANTILE_NAME

            if verbose:
                print("    scheduling job to combine quantiles")

            _names = []
            _paths = []
            for name, path in qnt_paths:
                _names.append(name)
                _paths.append(path)

            # generate a cache of PSD quantiles for this job
            qnt_cache = os.path.join(logdir, 'psd-quantiles.txt')
            if verbose:
                print("writing individual PSD quantile paths to : %s" % (qnt_cache))
            with open(qnt_cache, 'w') as ect:
                ect.write('\n'.join(_paths))

            # add job to DAG
            cmb_qnt_path = os.path.join(
                output_dir,
                COMBINE_QUANTILE_NAME+'.hdf',
            )

            obj.write(JOB % {'name':COMBINE_QUANTILE_NAME, 'sub':cmb_qnt, 'retry':retry})
            obj.write(VARS % {
                'name':COMBINE_QUANTILE_NAME,
                'vars': 'COMBINE_QNT_PATH="%s" QNT_CACHE="%s"' \
                    % (cmb_qnt_path, qnt_cache),
            })

            # add parent-child relationships
            for name in _names:
                obj.write(PARENT_CHILD % {'parent':name, 'child':COMBINE_QUANTILE_NAME})

            # generate shell script
            if generate_shell_scripts:
                scripts.append(combine_quantile_shell(
                    [], # qnt_paths, should be an iterable
                    cmb_qnt_path,
                    logdir,
                    qnt_cache=qnt_cache,
                    verbose=verbose,
                ))

        # schedule job to extract median
        if verbose:
            print("    scheduling job to extract representative PSD as quantile=%.3f" % representative_quantile)

        rep_path = os.path.join(
            output_dir,
            REPRESENTATIVE_PSD_NAME+'.csv.gz',
        )

        obj.write(JOB % {'name':REPRESENTATIVE_PSD_NAME, 'sub':rep_psd, 'retry':retry})
        obj.write(VARS % {
            'name':REPRESENTATIVE_PSD_NAME,
            'vars': 'QUANTILE_PATH="%s" QUANTILE="%.9e" PSD_PATH="%s"' \
                % (cmb_qnt_path, representative_quantile, rep_path),
        })

        # add parent-child relationships
        obj.write(PARENT_CHILD % {'parent':cmb_qnt_name, 'child':REPRESENTATIVE_PSD_NAME})

        # generate shell script
        if generate_shell_scripts:
            scripts.append(representative_psd_shell(
                cmb_qnt_path,
                representative_quantile,
                rep_path,
                logdir,
                verbose=verbose,
            ))

    # make a big script with all jobs in order
    if generate_shell_scripts:
        estimate_psd_workflow(
            scripts,
            logdir,
        )

    #--------------------

    if verbose: ### report amount of time covered
        print( 'requested       : %d sec' % (gpsstop-gpsstart) )
        print( 'within segments : %d sec' % livetime(segs) )
        print( 'covered by PSD  : %d sec (%d PSDs)' % (covered, int(covered//window)) )

    #--------------------

    return dag

#-------------------------------------------------
#
# Logic for generating timeseries data
#
#-------------------------------------------------

HDF2SERIES_FILENAME = 'mcvt-hdf2series'
HDF2SERIES_NAME = HDF2SERIES_FILENAME+'-%d-%d'

DEFAULT_HDF2SERIES_UNIVERSE = 'vanilla'
DEFAULT_HDF2SERIES_REQUEST_MEMORY = 1000 # 10000 MB should hopefully be enough for most jobs?
DEFAULT_HDF2SERIES_REQUEST_CPUS = 1
DEFAULT_HDF2SERIES_REQUEST_DISK = 1000 # 10000 MB should be enough for most jobs?

#------------------------

def hdf2series_sub(
        network,
        outdir,
        logdir,
        accounting_group,
        frame_name=None,
        frame_path_suffixes=None,
        output_dir_mod=None,
        frame_sample_rate=None,
        frame_duration=None,
        file_duration=None,
        names=None,
        channel_names=None,
        frame_start_reference=None,
        waveform_flow=None,
        waveform_fref=None,
        waveform_tref_convention=None,
        waveform_duration_stretch=None,
        waveform_duration_padding=None,
        waveform_half_tukey_highpass_knee=None,
        waveform_butterworth_highpass_knee=None,
        waveform_butterworth_highpass_order=None,
        waveform_ramp_up_half_tukey_alpha=None,
        waveform_ramp_up_half_tukey_duration=None,
        waveform_ramp_down_half_tukey_alpha=None,
        waveform_ramp_down_half_tukey_duration=None,
        verbose=False,
        Verbose=False,
        accounting_group_user=DEFAULT_ACCOUNTING_GROUP_USER,
        universe=DEFAULT_HDF2SERIES_UNIVERSE,
        request_memory=DEFAULT_HDF2SERIES_REQUEST_MEMORY,
        request_cpus=DEFAULT_HDF2SERIES_REQUEST_CPUS,
        request_disk=DEFAULT_HDF2SERIES_REQUEST_DISK,
    ):

    path = os.path.join(logdir, HDF2SERIES_FILENAME+'.sub')

    #---

    # set up arguments

    ### arguments passed from within DAG
    args = ['$(SAMPLES)', network, '--gps-start', '$(GPS_START)', '--gps-end', '$(GPS_END)']

    ### fixed arguments (declared in SUB)
    if names is not None:
        for a, b in names:
            args += ['--name', a, b]

    if frame_name is not None:
        args += ['--frame-name', frame_name]

    if frame_path_suffixes is not None:
        for suffix in frame_path_suffixes:
            args += ['--frame-path-suffix', suffix]

    args += ['--output-dir', outdir]

    if output_dir_mod is not None:
        args += ['--output-dir-mod', '%d'%output_dir_mod]

    if frame_sample_rate is not None:
        args += ['--frame-sample-rate', '%d'%frame_sample_rate]

    if frame_duration is not None:
        args += ['--frame-duration', '%d'%frame_duration]

    if file_duration is not None:
        args += ['--file-duration', '%d'%file_duration]

    if channel_names is not None:
        for a, b in channel_names:
            args += ['--channel-name', a, b]

    if frame_start_reference is not None:
        args += ['--frame-start-reference', '%d'%frame_start_reference]

    if waveform_flow is not None:
        args += ['--waveform-flow', '%.9f'%waveform_flow]

    if waveform_fref is not None:
        args += ['--waveform-fref', '%.9f'%waveform_fref]

    if waveform_tref_convention is not None:
        args += ['--waveform-tref-convention', waveform_tref_convention]

    if waveform_duration_stretch is not None:
        args += ['--waveform-duration-stretch', '%.9f'%waveform_duration_stretch]

    if waveform_duration_padding is not None:
        args += ['--waveform-duration-padding', '%.9f'%waveform_duration_padding]

    if waveform_half_tukey_highpass_knee is not None:
        args += ['--waveform-half-tukey-highpass-knee', '%.9f'%waveform_half_tukey_highpass_knee]

    if waveform_butterworth_highpass_knee is not None:
        args += ['--waveform-butterworth-highpass-knee', '%.9f'%waveform_butterworth_highpass_knee]

    if waveform_butterworth_highpass_order is not None:
        args += ['--waveform-butterworth-highpass-order', '%.9f'%waveform_butterworth_highpass_order]

    if waveform_ramp_up_half_tukey_alpha is not None:
        args += ['--waveform-ramp-up-half-tukey-alpha', '%.9f'%waveform_ramp_up_half_tukey_alpha]

    if waveform_ramp_up_half_tukey_duration is not None:
        args += ['--waveform-ramp-up-half-tukey-duration', '%.9f'%waveform_ramp_up_half_tukey_duration]

    if waveform_ramp_down_half_tukey_alpha is not None:
        args += ['--waveform-ramp-down-half-tukey-alpha', '%.9f'%waveform_ramp_down_half_tukey_alpha]

    if waveform_ramp_down_half_tukey_duration is not None:
        args += ['--waveform-ramp-down-half-tukey-duration', '%.9f'%waveform_ramp_down_half_tukey_duration]

    if Verbose:
        args += ['--Verbose']

    elif verbose:
        args += ['--verbose']

    #---

    # write the file
    return sub(
        path,
        'mcvt-hdf2series',
        args,
        '$(LOGDIR)',
        accounting_group,
        accounting_group_user=accounting_group_user,
        universe=universe,
        request_memory=request_memory,
        request_cpus=request_cpus,
        request_disk=request_disk,
        verbose=True,
    )

#------------------------

def hdf2series_shell(
        hdf2series_name,
        samples,
        network,
        outdir,
        logdir,
        gps_start=None,
        gps_end=None,
        names=None,
        frame_name=None,
        frame_path_suffixes=None,
        output_dir_mod=None,
        frame_sample_rate=None,
        frame_duration=None,
        file_duration=None,
        channel_names=None,
        frame_start_reference=None,
        waveform_flow=None,
        waveform_fref=None,
        waveform_tref_convention=None,
        waveform_duration_stretch=None,
        waveform_duration_padding=None,
        waveform_half_tukey_highpass_knee=None,
        waveform_butterworth_highpass_knee=None,
        waveform_butterworth_highpass_order=None,
        waveform_ramp_up_half_tukey_alpha=None,
        waveform_ramp_up_half_tukey_duration=None,
        waveform_ramp_down_half_tukey_alpha=None,
        waveform_ramp_down_half_tukey_duration=None,
        verbose=False,
        Verbose=False,
    ):

    path = os.path.join(logdir, hdf2series_name+'.sh')

    #---

    args = [samples, network]

    if gps_start is not None:
        args += ['--gps-start', '%d'%gps_start]

    if gps_end is not None:
        args += ['--gps-end', '%d'%gps_end]

    if names is not None:
        for a, b in names:
            args += ['--name', a, b]

    if frame_name is not None:
        args += ['--frame-name', frame_name]

    if frame_path_suffixes is not None:
        for suffix in frame_path_suffixes:
            args += ['--frame-path-suffix', suffix]

    args += ['--output-dir', outdir]

    if output_dir_mod is not None:
        args += ['--output-dir-mod', '%d'%output_dir_mod]

    if frame_sample_rate is not None:
        args += ['--frame-sample-rate', '%d'%frame_sample_rate]

    if frame_duration is not None:
        args += ['--frame-duration', '%d'%frame_duration]

    if file_duration is not None:
        args += ['--file-duration', '%d'%file_duration]

    if channel_names is not None:
        for a, b in channel_names:
            args += ['--channel-name', a, b]

    if frame_start_reference is not None:
        args += ['--frame-start-reference', '%d'%frame_start_reference]

    if waveform_flow is not None:
        args += ['--waveform-flow', '%.9f'%waveform_flow]

    if waveform_fref is not None:
        args += ['--waveform-fref', '%.9f'%waveform_fref]

    if waveform_tref_convention is not None:
        args += ['--waveform-tref-convention', waveform_tref_convention]

    if waveform_duration_stretch is not None:
        args += ['--waveform-duration-stretch', '%.9f'%waveform_duration_stretch]

    if waveform_duration_padding is not None:
        args += ['--waveform-duration-padding', '%.9f'%waveform_duration_padding]

    if waveform_half_tukey_highpass_knee is not None:
        args += ['--waveform-half-tukey-highpass-knee', '%.9f'%waveform_half_tukey_highpass_knee]

    if waveform_butterworth_highpass_knee is not None:
        args += ['--waveform-butterworth-highpass-knee', '%.9f'%waveform_butterworth_highpass_knee]

    if waveform_butterworth_highpass_order is not None:
        args += ['--waveform-butterworth-highpass-order', '%.9f'%waveform_butterworth_highpass_order]

    if waveform_ramp_up_half_tukey_alpha is not None:
        args += ['--waveform-ramp-up-half-tukey-alpha', '%.9f'%waveform_ramp_up_half_tukey_alpha]

    if waveform_ramp_up_half_tukey_duration is not None:
        args += ['--waveform-ramp-up-half-tukey-duration', '%.9f'%waveform_ramp_up_half_tukey_duration]

    if waveform_ramp_down_half_tukey_alpha is not None:
        args += ['--waveform-ramp-down-half-tukey-alpha', '%.9f'%waveform_ramp_down_half_tukey_alpha]

    if waveform_ramp_down_half_tukey_duration is not None:
        args += ['--waveform-ramp-down-half-tukey-duration', '%.9f'%waveform_ramp_down_half_tukey_duration]

    if Verbose:
        args += ['--Verbose']

    elif verbose:
        args += ['--verbose']

    # write the file
    return shell(
        path,
        'mcvt-hdf2series',
        args,
        verbose=True,
    )

#-------------------------------------------------

def hdf2series_workflow(scripts, logdir):
    path = os.path.join(logdir, 'mcvt-condor-hdf2series.sh')
    return workflow(path, scripts, verbose=True)

SAFETY_BUFFER = 10 # sec, used to match events to subsets based on the time the cover
DEFAULT_NUM_FILES_PER_JOB = 10

def hdf2series_dag(
        samples,
        gps_start,
        gps_end,
        frame_sample_rate,
        file_duration,
        waveform_duration_stretch,
        waveform_duration_padding,
        network,
        outdir,
        logdir,
        accounting_group,
        num_files_per_job=DEFAULT_NUM_FILES_PER_JOB,
        names=None,
        frame_name=None,
        frame_path_suffixes=None,
        output_dir_mod=DEFAULT_MODDIR,
        frame_duration=None,
        channel_names=None,
        frame_start_reference=None,
        waveform_flow=None,
        waveform_fref=None,
        waveform_tref_convention=None,
        waveform_half_tukey_highpass_knee=None,
        waveform_butterworth_highpass_knee=None,
        waveform_butterworth_highpass_order=None,
        waveform_ramp_up_half_tukey_alpha=None,
        waveform_ramp_up_half_tukey_duration=None,
        waveform_ramp_down_half_tukey_alpha=None,
        waveform_ramp_down_half_tukey_duration=None,
        verbose=False,
        Verbose=False,
        accounting_group_user=DEFAULT_ACCOUNTING_GROUP_USER,
        universe=DEFAULT_HDF2SERIES_UNIVERSE,
        request_memory=DEFAULT_HDF2SERIES_REQUEST_MEMORY,
        request_cpus=DEFAULT_HDF2SERIES_REQUEST_CPUS,
        request_disk=DEFAULT_HDF2SERIES_REQUEST_DISK,
        generate_shell_scripts=False,
        retry=DEFAULT_RETRY,
    ):
    
    assert len(samples), 'no samples provided, so there is nothing to do!'

    if gwdio is None:
        raise ImportError('could not import gwdistributions.gwdio')

    #---

    # make sure we have full paths
    outdir = os.path.abspath(outdir)
    logdir = os.path.abspath(logdir)

    # write the sub file
    sub = hdf2series_sub(
        network,
        outdir,
        logdir,
        accounting_group,
        names=names,
        frame_name=frame_name,
        frame_path_suffixes=frame_path_suffixes,
        output_dir_mod=output_dir_mod,
        frame_sample_rate=frame_sample_rate,
        frame_duration=frame_duration,
        file_duration=file_duration,
        channel_names=channel_names,
        frame_start_reference=frame_start_reference,
        waveform_flow=waveform_flow,
        waveform_fref=waveform_fref,
        waveform_tref_convention=waveform_tref_convention,
        waveform_duration_stretch=waveform_duration_stretch,
        waveform_duration_padding=waveform_duration_padding,
        waveform_half_tukey_highpass_knee=waveform_half_tukey_highpass_knee,
        waveform_butterworth_highpass_knee=waveform_butterworth_highpass_knee,
        waveform_butterworth_highpass_order=waveform_butterworth_highpass_order,
        waveform_ramp_up_half_tukey_alpha=waveform_ramp_up_half_tukey_alpha,
        waveform_ramp_up_half_tukey_duration=waveform_ramp_up_half_tukey_duration,
        waveform_ramp_down_half_tukey_alpha=waveform_ramp_down_half_tukey_alpha,
        waveform_ramp_down_half_tukey_duration=waveform_ramp_down_half_tukey_duration,
        verbose=verbose,
        Verbose=Verbose,
        accounting_group_user=accounting_group_user,
        universe=universe,
        request_memory=request_memory,
        request_cpus=request_cpus,
        request_disk=request_disk,
    )

    #---

    ### figure out how many jobs we need

    assert (gps_end - gps_start) % file_duration == 0, 'gps_end-gps_start must be an integer multiple of file_duration'

    edges = list(range(gps_start, gps_end, file_duration*num_files_per_job)) # the start times for each file
    edges = list(zip(edges, edges[1:] + [gps_end]))

    num_subsets = len(edges)

    if verbose:
        print('generating workflow to create files between %d - %d (each of length %d sec) within %d jobs' % \
            (gps_start, gps_end, file_duration, num_subsets))

    #---

    ### figure out which samples will overlap with which jobs, and write subsets of samples for each job

    subsets = [[] for _ in range(num_subsets)] # lists of events to be written for each subset

    # set up what we need to estimate signal duration

    if names:
        if gwdist_names is None:
            raise ImportError('could not import gwdistributions.backends.names')

        for old, new in names:
            gwdist_names.use(old, new, verbose=Verbose)

    # sort events by their gps time at geocenter (smallest to largest)
    samples.sort(key = lambda event: event[gwdist_names.name('geocenter_time')])

    fNyquist = frame_sample_rate/2 # needed when estimating duration

    # iterate over samples and assign them to subsets

    if verbose:
        print('mapping %d samples into %d subsets' % (len(samples), num_subsets))

    for evtind, event in enumerate(samples):

        dur = event2duration(
            event,
            flow=waveform_flow,
            fhigh=fNyquist,
            stretch=waveform_duration_stretch,
            padding=waveform_duration_padding,
        )

        ### finally, determine the window of interest around this event
        gps = event[gwdist_names.name('geocenter_time')]
        event_start = gps - dur - SAFETY_BUFFER
        event_end = gps + SAFETY_BUFFER

        # iterate over edges and check if this event will overlap with each
        for subind, ((start, end), subset) in enumerate(zip(edges, subsets)):

            ### determine if there is overlap
            overlap = (event_start <= start) and (start <= event_end)
            overlap |= (event_start <= end) and (end <= event_end)
            overlap |= (start <= event_start) and (event_start <= end)
            overlap |= (start <= event_end) and (event_end <= end)

            if overlap:
                if Verbose:
                    sys.stdout.write('\r    adding event %6d (%.3f - %.3f) to subset %6d (%d - %d)' % \
                        (evtind, event_start, event_end, subind, start, end))
                    sys.stdout.flush()
                subset.append(event)

    if Verbose:
        sys.stdout.write('\n')
        sys.stdout.flush()

    #---

    ### iterate over jobs and include each in the DAG 

    dag = os.path.join(logdir, 'mcvt-condor-hdf2series.dag')
    if verbose:
        print("writing : "+dag)

    if generate_shell_scripts:
        scripts = []

    # needed when writing subsets to disk
    scalars = samples[0].scalar_attributes
    vectors = samples[0].vector_attributes

    # now, actually iterate
    with open(dag, 'w') as obj:
        for subind, ((start, end), subset) in enumerate(zip(edges, subsets)):

            if verbose:
                print('processing subset %3d / %3d (%d-%d)' % (subind, num_subsets, start, end-start))

            name = HDF2SERIES_NAME % (start, end-start)

            # make hierarchical directories
            smod = start // output_dir_mod
            sdir = 'mcvt-hdf2series-%06d' % smod

            this_logdir = os.path.join(logdir, sdir)
            os.makedirs(this_logdir, exist_ok=True)

            # write subset to disk
            subset_path = os.path.join(this_logdir, name+'.hdf')
            if verbose:
                print('writing %d samples to: %s' % (len(subset), subset_path))
            gwdio.events2file(subset, scalars, vectors, subset_path, gps_start=start, gps_end=end)

            # write job into dag
            obj.write(JOB % {'name':name, 'sub':sub, 'retry':retry})
            obj.write(VARS % {
                'name':name,
                'vars': 'SAMPLES="%s" GPS_START="%d" GPS_END="%d" LOGDIR="%s"' \
                    % (subset_path, start, end, this_logdir),
            })

            if generate_shell_scripts:
                scripts.append(hdf2series_shell(
                    name,
                    subset_path,
                    network,
                    outdir,
                    this_logdir,
                    gps_start=start,
                    gps_end=end,
                    names=names,
                    frame_name=frame_name,
                    frame_path_suffixes=frame_path_suffixes,
                    output_dir_mod=output_dir_mod,
                    frame_sample_rate=frame_sample_rate,
                    frame_duration=frame_duration,
                    file_duration=file_duration,
                    channel_names=channel_names,
                    frame_start_reference=frame_start_reference,
                    waveform_flow=waveform_flow,
                    waveform_fref=waveform_fref,
                    waveform_tref_convention=waveform_tref_convention,
                    waveform_duration_stretch=waveform_duration_stretch,
                    waveform_duration_padding=waveform_duration_padding,
                    waveform_half_tukey_highpass_knee=waveform_half_tukey_highpass_knee,
                    waveform_butterworth_highpass_knee=waveform_butterworth_highpass_knee,
                    waveform_butterworth_highpass_order=waveform_butterworth_highpass_order,
                    waveform_ramp_up_half_tukey_alpha=waveform_ramp_up_half_tukey_alpha,
                    waveform_ramp_up_half_tukey_duration=waveform_ramp_up_half_tukey_duration,
                    waveform_ramp_down_half_tukey_alpha=waveform_ramp_down_half_tukey_alpha,
                    waveform_ramp_down_half_tukey_duration=waveform_ramp_down_half_tukey_duration,
                    verbose=verbose,
                    Verbose=Verbose,
                ))

    #--------------------

    # make a big script with all jobs in order
    if generate_shell_scripts:
        hdf2series_workflow(
            scripts,
            logdir,
        )

    #--------------------

    return dag
