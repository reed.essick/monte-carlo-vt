"""a module that houses general utilities
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import numpy as np

#-------------------------------------------------

def event2duration(event, flow=None, fhigh=None, stretch=1.0, padding=0.0, verbose=False):
    """compute the duration to be used when generating time-domain waveforms
    """

    # estimate the signal duration
    duration = event._frequency_domain_waveform_duration(
        event['mass1_detector'], # the name should automatically resolve themselves
        event['mass2_detector'],
        flow if (flow is not None) else event['waveform_flow'],
        fhigh if (fhigh is not None) else event['waveform_flow'],
    )

    if verbose:
        print('        duration = %.6f sec' % duration)

    ### apply factors of safety
    duration *= stretch
    duration += padding

    if verbose:
        print('        stretched and padded duration = %.6f sec' % duration)

    ### find the smallest power of 2
    dur = 1.0 # always use at least 1 sec
    while dur < duration:
        dur *= 2

    if verbose:
        print('        selected duration = %.6f sec' % dur)

    return dur

#-------------------------------------------------

def match(source, result, error, verbose=False, Verbose=False):
    """return a mapping from the indeces in result to the corresponding indeces in source that match to within error
    """
    num_source = len(source)
    num_result = len(result)

    if verbose:
        print('matching %d results to %d original events with allowable error=%.3e' % \
            (num_result, num_source, error))

    order_source = np.argsort(source)
    order_result = np.argsort(result)

    osource = source[order_source] # ordered versions of the data
    oresult = result[order_result]

    # iterate to match found injections to their parameters
    src_ind = 0
    new_ind = 0

    source_inds = [] # holders for matches
    result_inds = []
    while (src_ind < num_source) and (new_ind < num_result):

        if osource[src_ind] < oresult[new_ind] - error: ### injection is too far behind, so increment
            if Verbose:
                print('    source (ind=%d --> %.3f) is too far behind result (ind=%d --> %.3f). Incrementing source' \
                    % (src_ind, osource[src_ind], new_ind, oresult[new_ind]))
            src_ind += 1

        elif oresult[new_ind] < osource[src_ind] - error: ### found injections are too far behind, so increment
            if Verbose:
                print('    result (ind=%d --> %.3f) is too far behind source (ind=%d --> %.3f). Incrementing result' \
                    % (new_ind, oresult[fnd_ind], src_ind, osource[inj_ind]))
            new_ind += 1

        else: ### times are close enough to delcare a match
            # check to make sure this association is unique
            if (src_ind+1 < num_source):
                assert osource[src_ind+1] > oresult[new_ind] + error, \
                    'multiple source associated with the same result (ind=%d --> %.3f)'%(new_ind, oresult[new_ind])

            if (new_ind+1 < num_result):
                assert oresult[new_ind+1] > osource[src_ind] + error, \
                    'multiple result associated with the same source (ind=%d --> %.3f)'%(src_ind, osource[src_ind])

            # record the match
            source_inds.append(order_source[src_ind])
            result_inds.append(order_result[new_ind])

            # increment indecies
            src_ind += 1
            new_ind += 1

    if verbose:
        print('    found %d matches' % len(result_inds))

    # return
    return np.array(result_inds), np.array(source_inds)
