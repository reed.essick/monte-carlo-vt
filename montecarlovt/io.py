"""a small library to handle the task of writing custom file formats to disk
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import os

from collections import defaultdict

import h5py
import numpy as np

# non-standard libraries
try:
    from gwdistributions.backends import names
except ImportError:
    names = None

try:
    from LDAStools import frameCPP as frcpp # used to write frames
except ImportError:
    frcpp = None

#-------------------------------------------------

DEFAULT_SERIES_SUFFIX = "gwf"

GWF_SERIES_SUFFIXES = ["gwf"]
HDF_SERIES_SUFFIXES = ["hdf", "hdf5", "h5"]
KNOWN_SERIES_SUFFIXES = GWF_SERIES_SUFFIXES + HDF_SERIES_SUFFIXES

#-------------------------------------------------

def float2sec_ns(gps):
    gps_int = int(gps)
    gps_ns = int(1e9 * (gps - gps_int)) # NOTE, this could introduce truncation error at nsec level
    return gps_int, gps_ns

def sec_ns2float(gps_sec, gps_ns):
    return gps_sec + 1e-9*gps_ns

#-------------------------------------------------

def _path2delimiter(path):
    if path.endswith('csv') or path.endswith('csv.gz'):
        delimiter=','
    else:
        delimiter = ' '
    return delimiter

def write_psd(path, freqs, psd, verbose=False):
    if verbose:
        print('writing psd (freqs: %.3f -> %.3f with deltaf=%.3f) to: %s' % \
            (freqs[0], freqs[-1], freqs[1]-freqs[0], path))

    delimiter = _path2delimiter(path)

    np.savetxt(
        path,
        np.transpose([freqs, psd]),
        comments='',
        delimiter=delimiter,
        header=delimiter.join(['frequency', 'psd']),
    )

def load_psd(path, verbose=False):
    if verbose:
        print('loading psd from: '+path)
    data = np.genfromtxt(path, delimiter=_path2delimiter(path), names=True)
    return data['frequency'], data['psd']

#------------------------

def write_psd_quantile(path, num_psds, freqs, quantiles, psd, verbose=False):
    if verbose:
        print('writing psd quantiles to: '+path)
    with h5py.File(path, 'w') as obj:
        obj.attrs.create('num_psds', num_psds)
        obj.create_dataset('quantiles', data=quantiles)
        obj.create_dataset('frequency', data=freqs)
        obj.create_dataset('psd', data=psd)

def load_psd_quantile(path, verbose=False):
    if verbose:
        print('loading psd quantiles from: '+path)
    with h5py.File(path, 'r') as obj:
        num_psds = obj.attrs['num_psds']
        quantiles = obj['quantiles'][:]
        freqs = obj['frequency'][:]
        psd = obj['psd'][:]

    return num_psds, freqs, quantiles, psd

#-------------------------------------------------

def write_series(path, *args, **kwargs):
    """write time series data to disk
    data should be a structured array (keys are channel names)
    gps_start and dt define the properties of the time span
    """
    suffix = path.split('.')[-1]
    if suffix in GWF_SERIES_SUFFIXES:
        return write_series_gwf(path, *args, **kwargs)

    elif suffix in HDF_SERIES_SUFFIXES:
        return write_series_hdf(path, *args, **kwargs)

    else:
        raise ValueError('suffix=%s for %s not understood! must be one of: %s' % \
            (suffix, path, ', '.join(KNOWN_SERIES_SUFFIXES)))

def load_series(path, *args, **kwargs):
    """read time series data from disk
    """
    suffix = path.split('.')[-1]
    if suffix in GWF_SERIES_SUFFIXES:
        return load_series_gwf(path, *args, **kwargs)

    elif suffix in HDF_SERIES_SUFFIXES:
        return load_series_hdf(path, *args, **kwargs)

    else:
        raise ValueError('suffix=%s for %s not understood! must be one of: %s' % \
            (suffix, path, ', '.join(KNOWN_SERIES_SUFFIXES)))

#-------------------------------------------------

def write_series_gwf(path, data, gps_start, dt, frame_dur=None, verbose=False):
    """write time series data into a GWF frame
    data should be a structured array (keys are channel names)
    gps_start and dt define the properties of the time span
    """

    if frcpp is None:
        raise ImportError('could not import LDAStools.frameCPP')

    #---

    num_samples = len(data)
    duration = num_samples * dt

    if frame_dur is None:
        frame_dur = duration

    else:
        assert (duration % frame_dur) == 0, \
        'duration=%d must be an integer multiple of frame_dur=%d' \
            % (duration, frame_dur)

    num_frames = int(duration // frame_dur) ### number of frames per file
    samples_per_frame = int(frame_dur / dt) ### number of samples per frame

    #---

    if verbose:
        print('writing series data (%d samples) as %d frames (%d samples/frame) to : %s' % (duration/dt, num_frames, samples_per_frame, path))

    stream = frcpp.OFrameFStream(path)

    for ind in range(num_frames):

        start = gps_start + ind*frame_dur

        samples_start = ind*samples_per_frame # use this to grab the appropriate sample set for this frame
        samples_end = samples_start + samples_per_frame

        if verbose:
            print('    writing frame %d : %d -> %d (%d : %d)' % (ind, start, start+frame_dur, samples_start, samples_end))

        # make the frame
        frame = frcpp.FrameH()

        # set basic parameters of the frame
        frame.SetName('%d-%d'%(start, frame_dur)) # NOTE: SetName could be fragile
        frame.SetGTime(frcpp.GPSTime(*float2sec_ns(start)))
        frame.SetDt(frame_dur)

        # iterate over data, adding each dtype as
        for channel in data.dtype.names:

            if verbose:
                print('        writing : '+channel)

            # allocate the vector
            frvect = frcpp.FrVect(
                channel,
                frcpp.FrVect.FR_VECT_8R,
                1,                      # number of dimensions
                frcpp.Dimension(        # the dimension object
                    samples_per_frame,  # number of elements
                    dt,                 # spacing
                    's',                # units
                    0,                  # start time offset relative to the header (set above)
                ),
                '',  # dimensionless unit for strain
            )

            # actually populate the object
            frvect.GetDataArray()[:] = data[channel][samples_start:samples_end]

            # add that vector to the ProcData
            frdata = frcpp.FrProcData(
                channel,
                '',                                 # comment
                frcpp.FrProcData.TIME_SERIES,       # ID as time-series
                frcpp.FrProcData.UNKNOWN_SUB_TYPE,  # empty sub-type (fseries)
                0,                                  # offset of first sample relative to frame start
                frame_dur,                          # duration of data
                0.0,                                # heterodyne frequency
                0.0,                                # phase of heterodyne
                0.0,                                # frequency range
                0.0,                                # resolution bandwidth
            )

            # add the vector to this data
            frdata.AppendData(frvect)

            # add ProcData to FrameH
            frame.AppendFrProcData(frdata)

        # write
        frame.Write(stream)

#------------------------

def load_series_gwf(path, verbose=False, channels=None, time_range=None):
    """load series from a GWF file
    """

    if frcpp is None:
        raise ImportError('could not import LDAStools.frameCPP')

    #---

    if verbose:
        print('loading series data from : '+path)

    stream = frcpp.IFrameFStream(path)

    gps_start = None
    dt = None

    #---

    toc = stream.GetTOC()

    channels_fr_data = [
        (toc.GetProc(), stream.ReadFrProcData),
        (toc.GetADC(), stream.ReadFrAdcData),
        (toc.GetSim(), stream.ReadFrSimData),
    ]

    #---

    seriesdict = defaultdict(list)

    num_samples = 0
    num_frames = 0

    frame_stop = None

    if time_range is None:
        data_start = -np.infty
        data_stop = +np.infty
    else:
        data_start, data_stop = time_range

    #---

    # iterate over frames

    frame = stream.ReadNextFrame()  # assume at least one frame
    while frame:

        if verbose:
            print('    loading frame %d' % num_frames)

        # check for time overlap
        frame_start = sec_ns2float(*frame.GetGTime())
        frame_dur = frame.GetDt()
        
        if frame_start >= data_stop: # the rest of the data is not relevant
            if verbose:
                print('        frame starts (%.3f) after requested range end (%.3f). Breaking loop' % \
                    (frame_start, data_stop))
            break

        elif frame_start + frame_dur <= data_start:
            if verbose:
                print('        frame ends (%.3f) before requested range begins (%.3f). Skipping' % \
                    (frame_start + frame_dur, data_start))

        else:
            if gps_start is None: # set this to be the earliest relevant time encountered
                gps_start = frame_start

            # sanity check for continuity
            if frame_stop is not None:
                assert frame_start == frame_stop, 'frames are not continuous! (stop=%.3f start=%.3f)' % (frame_stop, frame_start)

            frame_stop = frame_start + frame_dur

            if verbose:
                print('        frame spans [%.3f, %.3f) -> %.3f sec' % (frame_start, frame_stop, frame_dur))

            # extract data
            _num_samples = None

            for chans, fr_data in channels_fr_data:
                for channel in chans:

                    if (channels is not None) and (channel not in channels): ### skip this channel
                        if verbose:
                            print('        skipping : '+channel)
                        continue

                    elif verbose:
                        print('        reading : '+channel)

                    # magic number: one frame per stream...
                    try:
                        data = fr_data(num_frames, channel)
                    except ValueError as e:
                        print('frame %d spans [%.3f, %.3f) -> %.3f sec' % (num_frames, frame_start, frame_stop, frame_dur))
                        raise e

                    dim = data.data[0].GetDim(0)

                    # magic number: one FrVect wrapper per channel
                    data = data.data[0].GetDataArray()
                    if dt is None:
                        dt = dim.dx
                    else:
                        assert dt == dim.dx, 'dim.dx conflicts between channels! %d vs %d' % (dt, dim.dx)

                    if _num_samples is None:
                        _num_samples = len(data)
                    else:
                        assert _num_samples == len(data), 'conflict in lenth of data! %d vs %d' % (_num_samples, len(data))

                    assert frame_dur == _num_samples*dt, 'bad length for data!'

                    # add to counters
                    seriesdict[channel].append(np.array(data[:], dtype=float))

            # update counters

            if _num_samples is not None:
                num_samples += _num_samples

        num_frames += 1

        try:
            frame = stream.ReadNextFrame()  # assume at least one frame

        except IndexError: # signals that there are no more frames
            break

    assert len(seriesdict), 'no channels found!'

    # map dictionary to a structured array
    series = np.empty(num_samples, dtype=[(channel, float) for channel in seriesdict.keys()])

    for channel, data in seriesdict.items():
        series[channel] = np.concatenate(tuple(data))

    ### downselect data to match requested range
    times = gps_start + np.arange(num_samples)*dt
    series = series[(data_start <= times) * (times < data_stop)]
    gps_start = max(data_start, gps_start)

    # return
    return series, gps_start, dt

#------------------------

def load_series_gwf_sequence(frames, channel, start, stop, verbose=False, Verbose=False):
    """
    returns a numpy array of the data inculded in frames between start and stop
    CURRENTLY ASSUME CONTIGUOUS DATA, but we should check this
    """
    vecs = []
    dt = 0
    for frame, frame_start, frame_dur in frames:
        s = max(frame_start, start)
        d = min(frame_start+frame_dur,stop) - s
        if d > 0:
            if verbose:
                print('    reading  : '+frame)

            series, t0, dt = load_series_gwf(
                frame,
                channels=[channel],
                verbose=Verbose,
                time_range=(s, s+d),
            )

            # sanity check a bit
            assert t0 == s, 'mismatch in start time of frame! %.3f vs %.3f' % (t0, s)
            if d != len(series)*dt: 
                raise RuntimeError('missing data! series ends at %.3f but we have requested up to %d' % \
                    (t0+len(series)*dt, s+d))

            # filter data
            vecs.append( series[channel] ) ### we have already filtered requested data

        elif verbose:
            print('    skipping : '+frame)

    # concatenate separate arrays
    vec = np.concatenate(vecs)

    # sanity check that we got all the data we expected
    assert len(vec)*dt == (stop - start), 'data found does not cover the entire requested range!'

    # return
    return vec, dt

#-------------------------------------------------

def write_series_hdf(path, data, gps_start, dt, verbose=False, **kwargs):
    """write time series data into a HDF file
    data should be a structured array (keys are channel names)
    gps_start and dt define the properties of the time span
    """

    num_samples = len(data)
    duration = num_samples * dt

    #---

    if verbose:
        print('writing series data (%d samples) to : %s' % (duration/dt, path))

    with h5py.File(path, 'w') as obj:

        obj.attrs.create('gps_start', gps_start)
        obj.attrs.create('deltat', dt)

        # store data as a single dataset (structured array) rather than multiple dataset
        if verbose:
            for channel in data.dtype.names:
                print('        writing : '+channel)
        obj.create_dataset('series', data=data)

#------------------------

def load_series_hdf(path, verbose=False):
    """load series data from an HDF file
    """
    if verbose:
        print('loading series data from : '+path)

    with h5py.File(path, 'r') as obj:
        gps_start = obj.attrs['gps_start']
        dt = obj.attrs['deltat']

        series = obj['series'][:]

    # map dictionary to a structured array
    if verbose:
        for channel in series.dtype.names:
            print('    found : '+channel)

    return series, gps_start, dt

#-------------------------------------------------

# maps between LSC-HDF names to gw-distribution names
CBC_WAVEFORM_PARAMS_GROUP_NAME = 'cbc_waveform_params'

# map from lschdf names to internal names
CBC_WAVEFORM_REQUIRED_PARAMS = {
    'mass1_det' : 'mass1_detector',
    'mass2_det' : 'mass2_detector',
    'spin1x' : 'spin1x',
    'spin1y' : 'spin1y',
    'spin1z' : 'spin1z',
    'spin2x' : 'spin2x',
    'spin2y' : 'spin2y',
    'spin2z' : 'spin2z',
    'd_lum' : 'luminosity_distance',
    'ra' : 'right_ascension',
    'dec' : 'declination',
    'inclination' : 'inclination',
    'polarization' : 'polarization',
    'coa_phase' : 'coalescence_phase',
    'eccentricity' : 'eccentricity',
    't_co_gps' : 'time_geocenter',
    'cbc_model' : 'approximant',
    'f22_start' : 'waveform_flow',
    'f22_ref_spin' : 'waveform_fref',
    'longAscNodes' : 'longitude_of_ascending_nodes',
    'meanPerAno' : 'mean_periastron_advance',
}

# see https://lscsoft.docs.ligo.org/lalsuite/lalsimulation/_l_a_l_sim_inspiral_waveform_params_8c_source.html
CBC_WAVEFORM_OPTIONAL_PARAMS = {
    'PrecThresholdMband' : 'phenomxphm_mband_threshold',
    'ThresholdMband' : 'phenomxhm_mband_threshold',
    'lambda1' : 'tidal_deformability1',
    'lambda2' : 'tidal_deformability2',
    'dchim2' : 'dchim2',
    'dchi0' : 'dchi0',
    'dchi1' : 'dchi1',
    'dchi2' : 'dchi2',
    'dchi3' : 'dchi3',
    'dchi4' : 'dchi4',
    'dchi5' : 'dchi5',
    'dchi5L' : 'dchi5L',
    'dchi6' : 'dchi6',
    'dchi6L' : 'dchi6L',
    'dchi7' : 'dchi7',
}

# fields that we fill in with default values
CBC_SIM_DATA_GROUP_NAME = 'cbc_sim_data'

# map from lschdf names to internal names
CBC_SIM_REQUIRED_DATA = {
#    'delta_time_geocenter' : 't_co_gps_add',
#    'sim_id' : 'cbc_sim_id',
}

CBC_SIM_OPTIONAL_DATA = {
}

def write_lschdf(path, events, verbose=False, **meta):
    """write tabular data (numpy structured array) to LSC HDF file format
    https://git.ligo.org/waveforms/o4-injection-file/-/blob/main/FILES.md
    """
    if names is None:
        raise ImportError('could not import gwdistributions.backends.names')

    num_events = len(events)

    if verbose:
        print('writing %d events to %s' % (num_events, path))

    with h5py.File(path, 'w') as obj:

        # create attrs
        for key, val in [('file_format', 'lvk_o4_injection')] + list(meta.items()):

            if isinstance(val, (bytes, np.bytes_)):
                val = val.decode('utf-8') # change to a simpler encoding

            # handle strings in a special way
            if isinstance(val, str):
                dtype = h5py.string_dtype(encoding='utf-8')
            else:
                dtype = type(val)

            # record the data
            if verbose:
                print('creating attr : %s = %s\t%s' % (key, val, dtype))
            obj.attrs.create(key, val, dtype=dtype)

        # store parameters
        for group_name, param_sets in [
                (CBC_WAVEFORM_PARAMS_GROUP_NAME, [(CBC_WAVEFORM_REQUIRED_PARAMS, 0.0), (CBC_WAVEFORM_OPTIONAL_PARAMS, None)]),
                (CBC_SIM_DATA_GROUP_NAME, [(CBC_SIM_REQUIRED_DATA, 0.0), (CBC_SIM_OPTIONAL_DATA, None)]),
            ]:
            if verbose:
                print('processing group : %s' % group_name)
            group = obj.create_group(group_name)
            for params, default in param_sets:
                for key, val in params.items():
                    if verbose:
                        print('    checking : %s : %s <- %s' % (group_name, key, names.name(val)))

                    if names.name(val) in events.dtype.names:
                        data = events[names.name(val)]

                    elif default is not None:
                        if verbose:
                            print('        dataset does not exist! setting to: %s' % default)
                        data = np.empty(num_events, dtype=float)
                        data[:] = default

                    elif verbose: # default is None
                        print('        dataset does not exist! skipping')
                        continue

                    # handle strings in a special way
                    if data.dtype.type is np.string_:
                        data = [_.decode('utf-8') for _ in data]
                        dtype = h5py.string_dtype(encoding='utf-8')

                    else:
                        dtype = data.dtype

                    # record the data
                    if verbose:
                        print('        creating dataset : %s : %s\t%s' % (group_name, key, dtype))
                    group.create_dataset(key, data=data, dtype=dtype)
