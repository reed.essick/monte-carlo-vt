"""a module to house logic surrounding data discovery
strongly influenced by https://github.com/reedessick/exposure/blob/master/exposure/datafind.py
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import numpy as np

import subprocess as sp

### non-standard libraries
from . import segments

#-------------------------------------------------

### SEGMENT DISCOVERY

def retrieve_segments(
        gpsstart,
        gpsstop,
        include_path=None,
        exclude_path=None,
        include_segdb_flags=[],
        exclude_segdb_flags=[],
        verbose=False,
    ):
    stride = gpsstop - gpsstart

    segs = [[gpsstart, gpsstop]]

    if include_path:
        if verbose:
            print('including only data within segments from : '+include_path)
        seg = segments.andsegments(segs, np.genfromtxt(include_path))

    if exclude_path:
        if verbose:
             print('excluding data within segments from : '+exclude_path)
        segs = segments.nandsegments(segs, gpsstart, gpsstop, np.genfromtxt(args.exclude_path))

    segs = include_flags(segs, include_segdb_flags, gpsstart, stride, verbose=verbose)
    segs = exclude_flags(segs, exclude_segdb_flags, gpsstart, stride, verbose=verbose)

    return segs

#------------------------

query_cmd = "ligolw_segment_query_dqsegdb -q -t https://segments.ligo.org -a %(flag)s -s %(gpsstart)d -e %(gpsstop)d"
print_cmd = "ligolw_print -c start_time -c end_time -t segment".split()

def query_flag(flag, start, stride, verbose=False):
    ### FIXME: use the SegDb Python API directly instead of this hack...
    cmd = query_cmd%{
        'flag':flag,
        'gpsstart':start,
        'gpsstop':start+stride,
    }
    if verbose:
        print( cmd )
    segs = sp.Popen(cmd.split(), stdout=sp.PIPE).communicate()[0] # retrieve ligolw xml, returns bytes-like object
    segs = sp.Popen(print_cmd, stdin=sp.PIPE, stdout=sp.PIPE).communicate(segs)[0] # extract from ligolw xml
    segs = segs.decode('utf-8').strip('\n').split('\n') # convert to str and manipulate
    
    return [[int(_) for _ in seg.split(',')] for seg in segs if seg] # cast to ints and return

def include_flags(segs, flags, start, stride, verbose=False):
    for flag in flags:
        segs = segments.andsegments(segs, query_flag(flag, start, stride, verbose=verbose))

    return segs

def exclude_flags(segs, flags, start, stride, verbose=False):
    if not segs:
        return segs

    gpsstart = segs[0][0]
    gpsstop = segs[-1][1]
    for flag in flags:
        segs = segments.nandsegments(segs, gpsstart, gpsstop, query_flag(flag, start, stride, verbose=verbose))

    return segs

#-------------------------------------------------

### FRAME DISCOVERY

gw_data_find_cmd = "gw_data_find -o %(observatory)s --type %(frametype)s --url file -s %(gpsstart)d -e %(gpsstop)d"
def gw_data_find(ifo, ldr_type, start, stride, verbose=False):
    """a routine to automate calls to gw_data_find to get frame locations, starts, and durations
    """
    cmd = gw_data_find_cmd%{
        'observatory':ifo,
        'frametype':ldr_type,
        'gpsstart':start,
        'gpsstop':start+stride,
    }
    if verbose:
        print( cmd )
    found = sp.Popen(cmd.split(), stdout=sp.PIPE).communicate()[0].decode('utf-8')
    found = found.replace('file://localhost','').strip().split()

    frames = []
    for frame in [frame for frame in found if frame]:
        if verbose:
            print('    found : '+frame)
        s, d = segments.extract_start_dur(frame)
        frames.append( (frame, s, d) )
    return frames
