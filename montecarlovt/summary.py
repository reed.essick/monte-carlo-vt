"""diagnostic plots and sensitivity estimates to summarize the behavior of individual populations
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import os
import warnings

import numpy as np
#import h5py

from scipy.stats import gamma

import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
plt.rcParams['font.family'] = 'serif'
#plt.rcParams['text.usetex'] = True

#-------------------------------------------------

### general utilities

def errors(n, b, N):
    """return error estimates needed to put shading on histograms
    """
    n = np.clip(np.array(n, dtype=float), 0, 1)
    s = (n*(1-n)/N)**0.5
    x = []
    y = []
    dy = []
    for n, s, b, B in zip(n, s, b[:-1], b[1:]):
        x += [b,B]
        y += [n]*2
        dy += [s]*2
    y = np.array(y)
    dy = np.array(dy)
    return x, y, dy

#-------------------------------------------------

### basic counting experiments

def counting_statistics(events, total_generated, columns=[]):
    """compute the number of detected events, the acceptance fraction, and basic properties of the specified columns
    """
    n = len(events)
    a = float(n)/total_generated
    da = (a*(1-a)/total_generated)**0.5
    return n, a, da, dict((col, (np.min(events[col]), np.max(events[col]))) for col in columns)

def report_counting_statistics(num_found, accept_frac, daccept_frac, columns):
    print("""\
    number found        = %d
    acceptance fraction = %.3e +/- %.3e"""%(num_found, accept_frac, daccept_frac))
    for key, (m, M) in columns.items():
        print("""\
    min observed %s = %.3f
    max observed %s = %.3f"""%(key, m, key, M))

#-------------------------------------------------

def pdet(weights, Ninj):
    Nfnd = len(weights)

    ### compute point estimate
    _pdet = np.sum(weights)/Ninj

    ### estimate uncertainty
    sample_variance = np.sum(((Nfnd/Ninj)*weights - _pdet)**2) / (Nfnd-1)
    sigma_pdet = (sample_variance / Nfnd)**0.5 # convert to variance on pdet

    return _pdet, sigma_pdet

#------------------------

### sensitive volume

def sensitive_volume(weights, Ninj):
    """expect weights to carry units of volume
    """
    return pdet(weights, Ninj)

def report_sensitive_volume(title, vol, sigma_vol, unit='Mpc^3'):
    print("""\
    %s
        <V> = %.6e +/- %.6e [%s]"""%(title, vol, sigma_vol, unit))

#------------------------

### effective number of samples

def effective_sample_size(weights, Ninj):
    _pdet, _sigma_pdet = pdet(weights, Ninj)

    ### return
    return _pdet, _sigma_pdet, _pdet_sigma2neff(_pdet, _sigma_pdet), len(weights)

def _pdet_sigma2neff(pdet, sigma_pdet):
    return (pdet / sigma_pdet)**2

def report_effective_sample_size(title, pdet, sigma_pdet, neff, nfnd):
    print("""\
    %s
        P(det) = %.6e +/- %.6e
        Neff = %10.3f
        Nfnd = %6d
        efficiency = (Neff/Nfnd) = %.6e"""%(title, pdet, sigma_pdet, neff, nfnd, neff/nfnd))

#-------------------------------------------------

### plotting functionality

def savefig(fig, figtmp, figtypes=[], verbose=False, prefix='', **kwargs):
    for figtype in figtypes:
        figname = figtmp % figtype
        if verbose:
            print(prefix+'saving : '+figname)
        fig.savefig(figname, **kwargs)

def close(*args, **kwargs):
    plt.close(*args, **kwargs)

#------------------------

def plot_psd(
        freqs, psd,
        min_frequency=None,
        max_frequency=None,
        min_psd=None,
        max_psd=None,
        title=None,
        color=None,
        alpha=1.0,
        linestyle=None,
        marker=None,
        fig=None,
    ):
    """make a simple plot of a PSD
    """
    if fig is None:
        fig = plt.figure()
    ax = fig.gca()

    ax.plot(freqs, psd, color=color, alpha=alpha, linestyle=linestyle, marker=marker)

    if title is not None:
        ax.set_title(title)

    ax.set_xlabel('frequency [Hz]')
    ax.set_ylabel('Power Spectral Density [1/Hz]')

    ax.set_xscale('log')
    ax.set_yscale('log')

    ax.grid(True, which='both')

    if min_frequency is not None:
        ax.set_xlim(xmin=min_frequency)

    if max_frequency is not None:
        ax.set_xlim(xmax=max_frequency)

    if min_psd is not None:
        ax.set_ylim(ymin=min_psd)

    if max_psd is not None:
        ax.set_ylim(ymax=max_psd)

    return fig

#-------------------------------------------------

def samples2range(samples, log=False):
    m = np.min(samples)
    M = np.max(samples)

    if log:
        assert np.all(samples > 0), 'can only logscale positive-definite variables!'
        if m == M:
            dm = 0.05
        else:
            dm = (M/m)**0.05
        m /= dm
        M *= dm

    else:
        if m == M: # no dynamic range
            if m == 0: ### scaling won't work
                dm = 1.0
            else:
                dm = 0.05*m
        else:
            dm = (M-m)*0.05
        m -= dm
        M += dm

    return m, M

#------------------------

def stacked_histogram(samples, column, label=None, log=False, fig=None):
    """make cumulative and differential histograms of the detected distribution
    """
    if fig is None:
        fig = plt.figure(figsize=(4,6))
        ax1 = fig.add_subplot(2,1,1)
        ax2 = fig.add_subplot(2,1,2)
        xmin = +np.infty
        xmax = -np.infty

    else:
        ax1, ax2 = fig.axes
        xmin, xmax = ax1.get_xlim()

    # set up basic parameters

    Ndet = len(samples)
    weights = np.ones(Ndet, dtype=float)/Ndet
    kwargs = dict(histtype='step')

    m, M = samples2range(samples, log=log)
    xmin = min(xmin, m) # protect limits from previous calls
    xmax = max(xmax, M)

    if log:
        bins = np.logspace(np.log10(m), np.log10(M), min(500, max(10, int(Ndet**0.5))))
        cins = np.logspace(np.log10(m), np.log10(M), min(1000, 10*Ndet))
    else:
        bins = np.linspace(m, M, min(500, max(10, int(Ndet**0.5))))
        cins = np.linspace(m, M, min(1000, 10*Ndet))

    #--- cumulative histogram

    n, b, p = ax1.hist(
        samples,
        bins=cins,
        cumulative=-1,
        weights=weights,
        label='max{%s} = %.3f'%(column, np.max(samples)),
        **kwargs
    )
    color = p[0].get_edgecolor()

    # plot error estimates
    x, y, dy = errors(n, b, Ndet)
    ax1.fill_between(x, y-dy, y+dy, color=color[:3]+(0.5/3,), edgecolor='none')

    #--- differential histogram

    dx = bins[1] - bins[0]
    n, b, _ = ax2.hist(
        samples,
        bins=bins,
        color=color,
        weights=weights/dx,
        label=label,
        **kwargs
    )

    # plot error estimates
    x, y, dy = errors(n*dx, b, Ndet)
    ax2.fill_between(x, (y-dy)/dx, (y+dy)/dx, color=color[:3]+(0.5/3,), edgecolor='none')

    #--- decorate
    if log:
        ax1.set_xscale('log')
        ax2.set_xscale('log')

    ax1.set_xlim(xmin=xmin, xmax=xmax)
    ax1.set_ylim(ymin=0.0, ymax=1.0)

    ax2.set_xlim(ax1.get_xlim())
#    ax2.set_ylim(ymin=0.0)

    plt.setp(ax1.get_xticklabels(), visible=False)
    ax2.set_xlabel(column)

    ax1.set_ylabel('P(%s|det)'%column)
    plt.setp(ax1.get_yticklabels(), visible=False)

    ax2.set_ylabel('p(%s|det)'%column)
    plt.setp(ax2.get_yticklabels(), visible=False)

    ax1.grid(True, which='both')
    ax2.grid(True, which='both')

    for ax in [ax1, ax2]:
        ax.tick_params(
            left=True,
            right=True,
            top=True,
            bottom=True,
            direction='in',
            which='both',
        )

    plt.subplots_adjust(
        left=0.08,
        right=0.98,
        bottom=0.08,
        top=0.98,
        hspace=0.03,
    )

    ### return
    return fig

#------------------------

def corner(events, columns, column_labels, log=[], label=None, fig=None):
    ndim = len(columns)

    ranges = []
    for col in columns:
        ranges.append(samples2range(events[col], log=col in log))

    # iterate and make plot
    if fig is None:
        fig = plt.figure(figsize=(1.5*ndim, 1.5*ndim))

    Ndet = len(events)

    # iterate through diagonals to find ranges
    for row in range(ndim):
        ax = fig.add_subplot(ndim, ndim, 1 + row*ndim + row)

        xmin, xmax = ranges[row]
        bins = np.linspace(xmin, xmax, min(500, max(10, int(0.5*Ndet**0.5))))
        dx = bins[1] - bins[0]

        n, b, p = ax.hist(
            events[columns[row]],
            bins=bins,
            histtype='step',
            weights=np.ones(Ndet, dtype=float)/(Ndet*dx),
            label=label,
        )
        color = p[0].get_edgecolor()

        ### add error bars
        x, y, dy = errors(n*dx, b, Ndet)
        ax.fill_between(x, (y-dy)/dx, (y+dy)/dx, color=color, alpha=0.10)

        if row != ndim-1:
            plt.setp(ax.get_xticklabels(), visible=False)
        else:
            ax.set_xlabel(column_labels[columns[row]])
            plt.setp(ax.get_xticklabels(), rotation=90)

        plt.setp(ax.get_yticklabels(), visible=False)

        m, M = ax.get_xlim()
        ranges[row] = (min(m, xmin), max(M, xmax))

        ax.set_xlim(ranges[row])
#        ax.set_ylim(ymin=0.)

    # iterate again now that ranges have been updated
    for row in range(ndim):
        for col in range(row):
            ax = fig.add_subplot(ndim, ndim, 1 + row*ndim + col)

            ax.plot(
                events[columns[col]],
                events[columns[row]],
                color=color,
                alpha=max(0.05, 1./len(events)),
                marker='.',
                linestyle='none',
            )

            ax.set_xlim(ranges[col])
            ax.set_ylim(ranges[row])

            if row != ndim-1:
                plt.setp(ax.get_xticklabels(), visible=False)
            else:
                ax.set_xlabel(column_labels[columns[col]])
                plt.setp(ax.get_xticklabels(), rotation=90)

            if col != 0:
                plt.setp(ax.get_yticklabels(), visible=False)
            else:
                ax.set_ylabel(column_labels[columns[row]])

    # finish decorating
    for ax in fig.axes:
        ax.tick_params(
            left=True,
            right=True,
            top=True,
            bottom=True,
            direction='in',
            which='both',
        )

    plt.subplots_adjust(
        hspace=0.03,
        wspace=0.03,
        left=0.15,
        bottom=0.15,
        right=0.98,
        top=0.98,
    )

    ### return
    return fig, color

#-------------------------------------------------

# timeseries plots

def plot_timeseries(times, series, title=None, samples=None, **kwargs):
    """plot time-series and projected histograms with annotations and basic statistics
    """
    fig = plt.figure(figsize=(6,4))
#    fig = plt.figure()

    ax1 = fig.add_axes([0.10, 0.10, 0.64, 0.82])
    ax2 = fig.add_axes([0.75, 0.10, 0.23, 0.82])

    tmin = times[0]
    tmax = tmin + len(series)*(times[1]-times[0])

    # plot timeseries
    ax1.plot(times, series, **kwargs)

    ax1.set_xlabel('time [sec]')
    ax1.set_xlim(xmin=tmin, xmax=tmax)

    ymin, ymax = ax1.get_ylim()
    ylim = max(abs(ymin), abs(ymax))
    ax1.set_ylim(ymin=-ylim, ymax=+ymax)

    ### add annotations of sample locations
    if samples is not None:
        ylim = ax1.get_ylim()
        for x in samples:
            if (tmin <= x) and (x <= tmax):
                ax1.plot([x]*2, ylim, color='k', alpha=0.25)
        ax1.set_ylim(ylim)

    # plot histogram
    nbins = min(500, max(10, int(len(series)**0.5)))
    ax2.hist(series, bins=nbins, histtype='step', orientation='horizontal', log=True)

    ax2.set_ylim(ax1.get_ylim())

    plt.setp(ax2.get_xticklabels(), visible=False)
    plt.setp(ax2.get_yticklabels(), visible=False)

    ### add basic statistics
    ymin, ymax = ax2.get_ylim()
    dy = (ymax-ymin)*0.05

    xmin, xmax = ax2.get_xlim()
    dx = (xmax-xmin)*0.05

    label = 'ave = %.3e\nmed = %.3e\nstd = %.3e\nmax = %.3e\nmin = %.3e' % \
        (np.mean(series), np.median(series), np.std(series), np.max(series), np.min(series))
    ax2.text(xmax-dx, ymax-dy, label, ha='right', va='top')

    # decorate
    if title is not None:
        fig.suptitle(title)

    for ax in fig.axes:
        ax.tick_params(
            left=True,
            right=True,
            top=True,
            bottom=True,
            direction='in',
            which='both',
        )

    # return
    return fig
