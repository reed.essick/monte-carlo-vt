"""logic for dealing with segments
strongly influenced by https://github.com/reedessick/exposure/blob/master/exposure/utils.py
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import numpy as np

#-------------------------------------------------

def extract_start_dur(path, suffix='.gwf'):
    return [int(_) for _ in path[:-len(suffix)].split('-')[-2:]]

#-------------------------------------------------

def livetime(segs):
    return np.sum(e-s for s, e in segs)

def andsegments(list1, list2):
    """
    computes the intersection of 2 lists of segments, returning a new list
    taken from laldetchar.idq.event.andtwosegmentlists
    """
    newsegments = []
    index = 0
    for seg in list1:
        while index > 0 and list2[index][0] > seg[0]:
            index -= 1
        while index < len(list2) and list2[index][1] <= seg[0]:
            index += 1
        while index < len(list2) and list2[index][0] < seg[1]:
            newsegments.append([max(seg[0], list2[index][0]), min(seg[1], list2[index][1])])
            index += 1
        if index > 0:
            index -= 1
    return newsegments

def nandsegments(list1, start, stop, list2):
    """computes interesection of list1 and invsegments(list2)
    """
    return andsegments(list1, invsegments(start, stop, list2))

def invsegments(start, stop, segs):
    """
    takes the inverse of a list of segments between start and stop
    assumes that all segments are already between start and stop
    """
    if not segs:
        return [[start, stop]]

    newsegments = []
    for s, e in segs:
        if start < s <= stop:
            newsegments.append( [start, s] )
        start = e
    if e < stop:
        newsegments.append( [e, stop] )

    return newsegments

def mergesegments(segments):
    """
    assumes segments are in the correct order and non-overlapping.
    simply merges any touching segments into one bigger segemnt
    """
    if len(segments)<2:
        return segments

    segs = []
    s, e = segments[0]
    for S, E in segments[1:]:
        if e==S:
            e=E
        else:
            segs.append( [s,e] )
            s = S
            e = E
    segs.append( [s, e] )
    return segs

#def checksegments(seg, window=None):
#    """
#    ensures segment makes sense
#    if provided, segment is sliced so that we only keep the part that interesects with window=[start, end]
#    """
#    s, e = seg
#    if window:
#        start, end = window
#        if s < start:
#            s = start # truncate the start of seg
#        elif not (s < end):
#            return False # no overlap between current segment and window
#        if e > end:
#            e = end # truncate the end of seg
#        elif not (e > start):
#            return False # no overlap between currnet segment and window
#
#    if s < e:
#        return [s,e]
#    elif s > e:
#        raise ValueError("something is very wrong with segment generation... seg[1]=%.3f < seg[0]=%.3f"%tuple(seg))
#    else:
#        return False
