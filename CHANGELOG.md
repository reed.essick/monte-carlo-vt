https://git.ligo.org/reed.essick/monte-carlo-vt/-/merge_requests/34
  - set the reference frequency within waveform generation for frames to match what is specified within the input HDF file
  - add support for multiple conventions for how to align the waveforms, including using the reference time specified by lalsimulation during waveform generation
