# monte-carlo-vt

A configuration and deployment repo to control the calculation of Monte-Carlo estimates of VT.
Relies upon a few external libraries

  * [lalsuite](https://git.ligo.org/lscsoft/lalsuite)
  * [gw-distributions](https://git.ligo.org/reed.essick/gw-distributions)
  * [gw-detectors](https://git.ligo.org/reed.essick/gw-detectors)
  * [exposure](https://github.com/reedessick/exposure)

along with more standard libraries like numpy, scipy, healpy, etc.

## Installation

This package will **not** automatically install its dependencies, and users will have to clone and install each by hand with something like
```
git clone https://git.ligo.org/reed.essick/monte-carlo-vt
cd monte-carlo-vt
python setup.py install
```
## Configuration

The pipeline configuration is installable and therefore used as a default within this repo's single executable.
However, users can override that with their own configuration if desired.
Within `montecarlovt.ini`, definitions of science time are taken from the [JRPC O3 wiki](https://wiki.ligo.org/LSC/JRPComm/ObsRun3#Low_45latency_data).

## Executables

There are a two main executables included in this library, both of which ingest the same config file format.
Both executables should produce PSD estimates and Monte-Carlo samples in the same directory structure, meaning the online jobs should produce equivalent results as the offline jobs.
Full help-strings are reproduced below.


**monte-carlo-vt-online** (configure and launch streaming PSD and Monte-Carlo VT estimation jobs)
```
usage: monte-carlo-vt-online [-h] [--config CONFIG] [-v] [-V] [-p]
                             [--monitor-cadence MONITOR_CADENCE]

a basic scheduling script to manage processes associated with the Monte-Carlo
VT pipeline

optional arguments:
  -h, --help            show this help message and exit
  --config CONFIG       the configuration file used within streaming-monte-
                        carlo-vt with additions to control setup of PSD
                        estimation.
  -v, --verbose
  -V, --Verbose
  -p, --persist         have this process stick around and monitor the child
                        processes, killing all of them if any of them die.
  --monitor-cadence MONITOR_CADENCE
                        how often we check on processes if --persist is
                        supplied.
```

**monte-carlo-vt-offline** (configure and submit offline PSD and Monte-Carlo VT estimation jobs to Condor)
```
usage: monte-carlo-vt-offline [-h] [--config CONFIG] [-v] [-V]
                              gpsstart gpsstop

a basic scheduling script to manage processes associated with the Monte-Carlo
VT pipeline

positional arguments:
  gpsstart
  gpsstop

optional arguments:
  -h, --help       show this help message and exit
  --config CONFIG  the configuration file used within streaming-monte-carlo-vt
                   with additions to control setup of PSD estimation.
  -v, --verbose
  -V, --Verbose
  --do-not-submit  only write the DAG but do not submit it to Condor
```

Additionally, we provide a helper script to generate XML tables suitable for pipeline software injections based on the samples produced by the other two workflows.


**monte-carlo-vt-injections**
```
usage: a simple script to standardize queries for monte-carlo samples and turn them into siminspiral xml tables suitable for injecting into pipelines

positional arguments:
  gpsstart
  gpsstop

optional arguments:
  -h, --help            show this help message and exit
  --config CONFIG       the configuration file used within streaming-monte-
                        carlo-vt with additions to control setup of PSD
                        estimation.
  --downsample DOWNSAMPLE
  --output-path OUTPUT_PATH
  -v, --verbose
  --pdet-thr PDET_THR   the initial threshold used to prune samples
  --fractional-systematic-error FRACTIONAL_SYSTEMATIC_ERROR
                        the fraction of the statistical error tolerated as
                        systemtic error from truncating the samples at low
                        pdet. We will perform a bisection search over pdet
                        threshold to determine an appropriate threshold to
                        maintain this level of precision **after** first
                        applying --pdet-thr.
  --relative-error RELATIVE_ERROR
                        the relative error used in the termination condition
                        for the bisection search that determines the correct
                        pdet threshold to maintain --fractional-systematic-
                        error
  --attributes [ATTRIBUTES [ATTRIBUTES ...]]
                        a space delimited list of the attributes that will be
                        included in the xml file
```

## Tutorials

### launching an instance of the pipeline

Launching the pipeline is easy.
To use the default configuration, simply run
```
nohup monte-carlo-vt-online --persist &
```
and to specify a specific config file (e.g., write to a directory you own), you can specify it via
```
nohup monte-carlo-vt-online --config ${PATH_TO_YOUR_CONFIG} --persist &
```

Please note, specifying the `--persist` option will automatically terminate and clean up all child processes should *any* of them fail.
This is optional, but is expected to be useful in most cases.
For example, Nagios monitors would only need to check for the existence of `monte-carlo-vt-pipeline` to guarantee that all child processes are present when `--persist` is specified.

It is similarly easy to launch an offline workflow between a `GPS_START` and `GPS_STOP` time with
```
monte-carlo-vt-offline ${GPS_START} ${GPS_STOP}
```

### configuring the pipeline

An example configuration file is shown below, along with explanations of what each option does.
Each of the options in the `[general]` and each IFO section are required.
Users must also specify all the required parameters for each generator, although those may vary from generator to generator.

```
[general]

### basic output parameters

# the base directory of all output
rootdir = /home/reed.essick/isotropy/monte-carlo-vt

# a unique tag to identify this data set
tag = H1L1V1

### FFT and PSD estimation parameters

# the total amount of time used to estimate each PSD (in sec)
stride = 64

# the length of each individual FFT (in sec)
seglen = 4

# the overlap between neighboring FFTs (in sec)
overlap = 2

# a parameter controlling the roll-off of our (Tukey) windowing function
tukey_alpha = 0.25

### parameters to control real-time scheduling

# how far behind real-time processes should attempt to stay
delay = 0

# the maximum amount of time behind realtime processes will tolerate before 
# abandoning a stride
max_latency = 128

### parameters controlling monte-carlo sampling

# the threshold on **observed** network SNR for detection.
# This is used along with a non-central chi-squared distribution to compute 
# the probability of detection given the optimal (zero-noise) SNR
snr_threshold = 12.

# the minimum number of detectors that must be present 
# (ie, have PSD estimates) to process a stride
min_num_present = 2

# the minimum number of monte-carlo samples drawn per stride. 
# We've found that 500 samples usually corresponds to 20-25 sec runtime
min_num_samples = 500

# the systematic uncertainty tolerated as part of the monte-carlo sampling.
# is set to "inf", will ignore this conditional.
# otherwise, will attempt to sample until the fractional uncertainty in 
# the rate likelihood due to the monte-carlo integrals is below this level.
fractional_systematic_error = inf

### which interferometers to include

# the set of IFOs for which we compute PSDs
# Each element of the space-delimited list must have a corresponding section (see below)
ifos = H1 L1 V1

# the IFOs that are required to be present in order to generate monte-carlo 
# samples for a given stride
required_ifos =

### parameters controlling the sampling distribution

# the generators from which the parameters are drawn.
# each element of the space-delimited list must have a corresponding section (see below)
generators = ParetoRedshift ParetoFlatComponentMass ZeroSpin RandomOrientation ZeroEccentricity

# the transformations to map the parameters drawn from generators into all parameters needed.
# We require SourceMass2DetectorMass to always be supplied and either Redshift2LuminosityDistance or 
# LuminosityDistance2Redshift depending on whether a RedshifDistribution or 
# DistanceDistribution is specified as a generator.
transforms = Redshift2LuminosityDistance SourceMass2DetectorMass

### Section for offline distributed workflow configuration
[condor]

# the pool of compute nodes used within Condor
universe = vanilla

# the accounting group for these jobs
# if this is *not* specified, will default to the value defined in [exposure](https://github.com/reedessick/exposure).
accounting_group = ligo.sim.o3.cbc.bayesianpopulations.parametric

# the number of retry attempts for each job
# if this is *not* specified, will default to the value defined in [exposure](https://github.com/reedessick/exposure).
retry = 3

### Sections for each IFO

[H1]
# the h(t) channel name
channel = H1:GDS-CALIB_STRAIN

# online data discovery options
# the shared memory directory in which frames are discoverable in low-latency
shmdir = /dev/shm/llhoft

# the frame type, discoverable in low-latency shared memory
# if this is *not* specified, will default to the value assigned to `ldr_type` in offline data discovery
shm_ldr_type = H1_llhoft

# which state-vector bits are required to be active for the IFO to be in "science mode"
# specified as "channel bit_index", and users can specify an arbitrary number of bits
active_bits =
    H1:GDS-CALIB_STATE_VECTOR 0
    H1:GDS-CALIB_STATE_VECTOR 1
    H1:GDS-CALIB_STATE_VECTOR 2

# offline data discovery options
# the frame type, available offline via ldr (gw_data_find) servers
ldr_type = H1_HOFT_C00

# a space delimited list of flags in https://segments.ligo.org that must all be known and active for this IFO to be in science time
include_flag = H1:DMT-ANALYSIS_READY:1

# a space delimited list of flags in https://segments.ligo.org, none of which can be known and active for this IFO to be in science time
exclude_flag =

[L1]
channel = L1:GDS-CALIB_STRAIN

shmdir = /dev/shm/llhoft
shm_ldr_type = L1_llhoft
active_bits =
    L1:GDS-CALIB_STATE_VECTOR 0
    L1:GDS-CALIB_STATE_VECTOR 1
    L1:GDS-CALIB_STATE_VECTOR 2

ldr_type = L1_HOFT_C00
include_flag = L1:DMT-ANALYSIS_READY:1
exclude_flag =

[V1]
channel = V1:Hrec_hoft_16384Hz

shmdir = /dev/shm/llhoft
ldr_type = V1_llhoft
active_bits =
    V1:DQ_ANALYSIS_STATE_VECTOR 0
    V1:DQ_ANALYSIS_STATE_VECTOR 2
    V1:DQ_ANALYSIS_STATE_VECTOR 10

ldr_type = V1Online
include_flag = V1:ITF_SCIENCE
exclude_flag =

### Sections for each Generator

[ParetoRedshift]

# parameters of the ParetoRedshif distribution

z_min = 0.001
z_max = 0.1
index = 3

[ParetoFlatComponentMass]

# parameters of the ParetoFlatComponentMass distribution
min_bh_mass = 1.0
max_bh_mass = 50.0
power_idx = -2.3

min_q = 0.1
max_q = 1.0

[ZeroSpin]

[RandomOrientation]

[ZeroEccentricity]

```

### interacting with data products produced by the pipeline

#### loading a (subset) of samples from disk

The supporting libraries provide a few utility functions that should improve the user-experience when discovering and using monte-carlo samples to estimate the exposure to different populations.
We demonstrate how to discover samples in a few lines

```
from lal.gpstime import tconvert
from exposure import montecarlo
from gw_event_gen import eventgen

#-------------------------------------------------

### where the samples live
rootdir = "path/to/rootdir"
tag = "yourtag"

### the bounds of your query
start = float(tconvert('1 April 2019')) ### the beginning of O3
stop = float(tconvert('now')) ### today

### retain only 1 out of this many samples
downsample = 10

### INI for your population model
### this should follow the same structure as the example config above
config = path/to/your/population.ini

#-------------------------------------------------

### create a generator with your target distribution
integrator = montecarlo.path2generator(config)

### go do a big I/O query
events = montecarlo.glob_samples(rootdir, tag, start, stop, downsample=downsample)
```

#### estimating the exposure from a set of samples

With these samples, one can then estimate the exposure.
If your preferred population model is implemented as a collection of `SamplingDistribution` objects, then this is as simple writing a config file, instantiating an `integrator`, and calling

```
### monte-carlo integrate over your samples
vt, uncertainty, effective_sample_size = integrator.integrate('pdet', events=events, sampling_pdf_attr='pdf')
```

Alternatively, if your preferred model is not implemented or you wish to access the samples through a numpy array instead of a list of `Event` objects, you can do the following

```
# a list of the attributes of each event you want to include in your structured array
attrs = ['mass1', 'mass2', 'z', 'pdet', 'pdf']
# make a structured array
samples = eventgen._events2array(events, attrs)
```

Estimates of monte-carlo integrals over the population are then immediately accessible with an additional function for the "new" population via

```
# in general, this will be some function of the attributes that represent the "new" population.
# I'm just taking the value of the pdf stored as an attribute as an example
new_pdf = lambda x: x['pdf']

# the weights assigned to each monte-carlo sample (multiply by new population and divide by the old)
weights = new_pdf(samples)/samples['pdf']


# again, in general this could be any function of the sample parameters we wish to monte-carlo integrate.
# I'm just using the probability that this sample would be detected (marginalizing over noise realizations)
# as an example.
foo = samples['pdet']

# actually perform the monte-carlo integration
vt, uncertainty, effective_sample_size = eventgen.montecarlo_integrate(foo, weights=weights)
```

*Note*, calls to `integrator.integrate` delegates to this function after setting up `foo` and `weights` internally.

If you wish to write the (downselected) samples to disk so you can access them in another language (e.g., stan), simply do

```
# a list of the attributes of each event you want to include in your structured array
attrs = ['mass1', 'mass2', 'z', 'pdet', 'pdf']

# the output file name
path = "path/to/output.csv.gz"

# write the samples into a comma-separated-value file
eventgen.events2csv(events, attrs, path, delimiter=',')

# or perhaps your preferred format is HDF5
eventgen.events2hdf5(events, attrs, path)

# or event sim-insprial LIGOLW xml tables suitable for defining injection sets for pipelines
### PLEASE NOTE! this makes it trivial to prepare reasonable populations for pipeline injections from data-informed population models
eventgen.events2xml(events, attrs, path)
```

#### changing the detection threshold (on network SNR) for a pre-exising set of samples

It may be the case that users wish to explore the impact of the precise threshold used when computing the probability of detection (marginalizing over noise realizations).
This can be done without re-generating new Monte-Carlo samples from the defined populations.
Instead, we simply re-compute `pdet` for existing samples and save the result with something like the following

```
from gw_event_gen.eventgen import Pdet ### import the transform, which relies on `net_snr`
from exposure import montecarlo

### go find samples
events = montecarlo.glob_samples(rootdir, tag, start, stop, downsample=downsample)

### define the transform
pdet = Pdet(snr_thr=8.) ### set the new SNR threshold while instantiating the transform

### iterate over events, re-calculating and storing pdet for each sample
for event in events:
    pdet(event) ### updates an attribute of event in-place

### you can then convert the samples to an array or save them to disk as needed
### see previous tutorials for details
```

### Generating injection sets for pipelines

To generate a LIGOLW SimInspiral XML table suitable for pipeline injections, simply run
```
monte-carlo-vt-injections ${START} ${STOP}
```
However, **please note** that you will almost certainly want to supply `--downselect` and `--pdet-thr` to select a reasonable subset of injections which have a chance of being detected by the pipeline.
The results will be written into the path specified via `--output-path`.
