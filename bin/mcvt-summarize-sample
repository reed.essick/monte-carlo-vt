#!/usr/bin/env python3

"""a script that makes some diagnostic plots and sensitivity estimates to summarize the behavior of individual populations
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import os
import warnings

import numpy as np

from argparse import ArgumentParser

### non-standard libraries
from gwdistributions.backends import names
from gwdistributions import parse
from gwdistributions import gwdio
from gwdistributions.utils.cosmology import Cosmology

from montecarlovt import summary

#-------------------------------------------------

parser = ArgumentParser(description=__doc__)

### arguments about events and populations
egroup = parser.add_argument_group('population options')

egroup.add_argument('events', type=str, help='path to HDF summary of semianalytic injections')

egroup.add_argument('-s', '--snr-threshold', default=[], nargs=2, type=str, action='append',
    help='a key and a threshold on SNR to include within the summary. Can be repeated. \
e.g. "--snr-threshold snr_net 10"')

### options about which columns to summarize
cgroup = parser.add_argument_group('options about individual columns')

cgroup.add_argument('--column', default=[], type=str, action='append',
    help='include this column in the plots plot. Can be repeated. \
If not supplied, defaults to a standard set of expected columns')
cgroup.add_argument('--label', nargs=2, type=str, default=[], action='append',
    help='the label for a particular column. If using tex, do not include $\'s. \
eg --label chirp_mass \mathcal{M}')
cgroup.add_argument('--log', type=str, default=[], action='append',
    help='log-scale x-axis in the 1D histogram and cumulative histogram for this column.')

### options about output
ogroup = parser.add_argument_group('output options')

ogroup.add_argument('-v', '--verbose', default=False, action='store_true')
ogroup.add_argument('-V', '--Verbose', default=False, action='store_true')

ogroup.add_argument('--plot-detected-distribution', default=False, action='store_true',
    help='plot distribution of detected events for each population')

ogroup.add_argument('--include-ndet', default=False, action='store_true',
    help='include the number of detected events within the legend for rate plots')
ogroup.add_argument('--include-neff', default=False, action='store_true',
    help='include the effective number of samples within the legend for rate plots')

ogroup.add_argument('-o', '--output-dir', default='.', type=str)
ogroup.add_argument('-t', '--tag', default='', type=str)
ogroup.add_argument('--figtype', default=[], type=str, action='append')
ogroup.add_argument('--dpi', default=200, type=float)

### finish parsing
args = parser.parse_args()

# event options
args.snr_threshold = [(key, float(val)) for key, val in args.snr_threshold]

# output options
args.verbose |= args.Verbose

args.output_dir = os.path.abspath(args.output_dir)

if args.tag:
    args.tag = "_"+args.tag

if not args.figtype:
    args.figtype.append('png')

# column options
ndim = len(args.column)

labels = dict((col, col) for col in args.column)
for col, label in args.label:
    assert col in args.column, 'specifying --label for unknown column=%s'%col
    labels[col] = label

for col in args.log:
    assert col in args.column, 'specifying --log for unknown column=%s'%col

#-------------------------------------------------

### load events
if args.verbose:
    print('loading events from: '+args.events)
events, meta = gwdio.hdf52array(args.events)

Ninj = meta['total_generated']

for col in args.column:
    assert col in events.dtype.names, 'column=%s not present!'%col

#------------------------

# compute boolean arrays for each snr threshold we want to consider
# retain only those selections that have some retained events
snr_threshold = []
for key, val in args.snr_threshold:
    sel = events[key] >= val
    if np.any(sel):
        snr_threshold.append( (key, val, sel) )

#-------------------------------------------------

if args.verbose:
    print('estimating statistics as a function of SNR threshold')

for key, thr, sel in snr_threshold:
    print('%s >= %.3f'%(key, thr))
    summary.report_counting_statistics(*summary.counting_statistics(events[sel], Ninj, columns=args.column))

#-------------------------------------------------

### plots

if args.plot_detected_distribution and args.column: ### at least one column to plot

    ### FIXME? also make plots for each population specified?

    ### 1D marginal distributions

    for col in args.column:
        if args.verbose:
            print('plotting 1D marginal distributions for : '+col)

        fig = None
        for key, thr, sel in snr_threshold:
            label = '%s >= %.3f : Ndet = %d'%(key, thr, np.sum(sel))
            fig = summary.stacked_histogram(events[sel][col], labels[col], label=label, log=col in args.log, fig=fig)

        # finish decorating
        ax1, ax2 = fig.axes
        ax1.legend(loc='upper right', fontsize=11)
        ax2.legend(loc='upper right', fontsize=11)

        # save
        figtmp = os.path.join(args.output_dir, 'mcvt-summarize-sample-p(%s|det)%s'%(col, args.tag)) + '.%s'
        for figtype in args.figtype:
            figname = figtmp % figtype
            if args.verbose:
                print('    saving: '+figname)
            fig.savefig(figname)
        summary.plt.close(fig)

    #--------------------

    ### corner plot
    if args.verbose:
        print('plotting corner for :\n    '+'\n    '.join(args.column))

    fig = None
    colors = []
    for key, thr, sel in snr_threshold:
        label = '%s >= %.3f : Ndet = %d'%(key, thr, np.sum(sel))
        fig, color = summary.corner(
            events[sel],
            args.column,
            column_labels=labels,
            label=label,
            log=args.log,
            fig=fig,
        )
        colors.append(color)
 
    # finish decorating
    for ind, (color, (key, thr, sel)) in enumerate(zip(colors, snr_threshold)):
        fig.text(
            0.98,
            0.98 - ind*0.05,
            r'%s >= %.1f : Ndet = %d'%(key, thr, np.sum(sel)),
            color=color,
            ha='right',
            va='top',
        )

    # save
    figtmp = os.path.join(args.output_dir, 'mcvt-summarize-sample-corner%s'%(args.tag)) + '.%s'
    for figtype in args.figtype:
        figname = figtmp % figtype
        if args.verbose:
            print('saving: '+figname)
        fig.savefig(figname)
    summary.plt.close(fig)
