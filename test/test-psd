#!/bin/bash

### a quick script to test the ability to estimate PSDs
### Reed Essick (reed.essick@gmail.com)

#-------------------------------------------------

CHANNEL="H1:GDS-CALIB_STRAIN"
FRAMETYPE="H1_HOFT_C00"

GPS_START=$(lal_tconvert Sat Apr 29 03:00:00 UTC 2023)
GPS_STOP=$(lal_tconvert Sat Apr 29 04:00:00 UTC 2023)

INCLUDE_FLAG="H1:DMT-ANALYSIS_READY"

#------------------------

MIN_PSD="1.0e-48"

MIN_FREQ="1.0"
MAX_FREQ="8192.0"

#------------------------

ACCOUNTING_GROUP="ligo.dev.o4.cbc.bayesianpopulations.parametric"

#-------------------------------------------------

#echo \
mcvt-estimate-psd \
    $CHANNEL $GPS_START $(($GPS_START + 60)) \
    --include-flag $INCLUDE_FLAG \
    --frametype $FRAMETYPE \
    --seglen 4 \
    --overlap 2 \
    --tukey-alpha 0.25 \
    --output-dir test-psd-output \
    --tag test-psd-output \
    --plot \
    --plot-min-psd $MIN_PSD \
    --plot-min-frequency $MIN_FREQ \
    --plot-max-frequency $MAX_FREQ \
    --Verbose \
|| exit 1

#------------------------

#echo \
mcvt-condor-estimate-psd \
    $CHANNEL $GPS_START $GPS_STOP \
    --include-flag $INCLUDE_FLAG \
    --frametype $FRAMETYPE \
    --seglen 4 \
    --overlap 2 \
    --tukey-alpha 0.25 \
    --output-dir test-psd-condor-output \
    --logdir test-psd-condor-log \
    --accounting-group $ACCOUNTING_GROUP \
    --generate-shell-scripts \
    --representative-psd-quantile 0.10 \
    --representative-psd-quantile 0.50 \
    --representative-psd-quantile 0.90 \
    --plot-representative-psd \
    --plot-min-psd $MIN_PSD \
    --plot-min-frequency $MIN_FREQ \
    --plot-max-frequency $MAX_FREQ \
    --Verbose \
|| exit 1
